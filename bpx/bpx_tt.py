import numpy as np
import tt
import copy
#import scipy.sparse.linalg
import scipy.sparse as sp
import scipy.sparse.linalg
import time


def _reshape(a, size):
    return a.reshape(size, order='f')


class MatMulTT:
    '''
        The class represents object that is a sum of products of matrices:
        c1 * A1.T @ Lam1 @ A1 + ... + cD * AD.T @ LamD @ AD
        Input:
            A = [A1,...,AD], Ri is a TT-matrix
            Lam = [Lam1,...,LamD], Ri is a TT-matrix
            c = [c1,...,cD], ci is a number
        By default c is None, which means ci = 1
    '''
    def __init__(self, A, Lam=None, c=None):
        self.A = A
        self.Lam = Lam
        self.D = len(A)
        self.d = A[0].tt.d
        if c is None:
            self.c = np.ones(self.D)
        else:
            self.c = c
        # Note that self.r[i] is a list of ranks
        # of the i-th mode for all [A1,...,AD]
        #self.n = zip(*[A[i].m for i in range(self.D)])
        self.n = A[0].n
        self.l = zip(*[A[i].n for i in range(self.D)])
        self.r = zip(*[A[i].tt.r for i in range(self.D)])
        self.cores = zip(*[tt.matrix.to_list(A[i]) for i in range(self.D)])
        if Lam:
            self.cores_Lam = zip(*[tt.matrix.to_list(Lam[i]) for i in range(self.D)])
            self.R = zip(*[Lam[i].tt.r for i in range(self.D)])


def als_solve_matmult(A, rhs, rank, tol,
                      x0=None, nswp=1, max_full_size=5000,
                      resid_damp=2.0, verb=1, local_iters=5,
                      local_restart=30, pinv=False):
    '''
        A must be a MatMulTT object
    '''
    n = rhs.n
    l = A.l # Internal index
    d = rhs.d
    rA = A.r
    if A.Lam:
        RA = A.R
    r_rhs = rhs.r
    D = A.D

    #rA_full = A_full.tt.r

    log = {}
    log['dx'] = []
    log['time'] = []
    log['res'] = []
    log['res_prev'] = []


    if not isinstance(rank, (list, tuple, np.ndarray)):
        rx = np.ones(d+1, dtype=np.int32)
        rx[1:d] = rank
    else:
        rx = copy.copy(rank)

    if x0 is None:
        x = tt.rand(n, r=rx)
    else:
        x = x0.copy()

    # Interfaces (to efficiently form matrices of local systems)
    phi_A = [None for i in range(d+1)]
    phi_A[0] = [np.ones((1, 1, 1, 1, 1)) for i in range(D)]
    phi_A[-1] = [np.ones((1, 1, 1, 1, 1)) for i in range(D)]

    phi_A_old = [None for i in range(d+1)]
    phi_A_old[0] = [np.ones((1, 1, 1, 1)) for i in range(D)]
    phi_A_old[-1] = [np.ones((1, 1, 1, 1)) for i in range(D)]

    phi_rhs = [None for i in range(d+1)]
    phi_rhs[0] = np.ones((1, 1))
    phi_rhs[-1] = np.ones((1, 1))

    x_cores = tt.vector.to_list(x)
    rhs_cores = tt.vector.to_list(rhs)
    A_cores = A.cores
    if A.Lam:
        Lam_cores = A.cores_Lam
    else:
        Lam_cores = None
    #A_cores_full = tt.matrix.to_list(A_full)

    for k in range(nswp):

        t1 = time.time()

        # Orthogonalization (<-) and computation of phi
        for i in reversed(range(1, d)):
            x_core = x_cores[i]
            x_core = _reshape(x_core, (rx[i], n[i]*rx[i+1]))
            x_core, R = np.linalg.qr(x_core.T)
            x_core2 = x_cores[i-1]
            x_core2 = _reshape(x_core2, (rx[i-1]*n[i-1], rx[i]))
            x_core2 = x_core2.dot(R.T)
            rx[i] = x_core.shape[1]
            x_core = _reshape(x_core.T, (rx[i], n[i], rx[i+1]))
            x_cores[i-1] = _reshape(x_core2, (rx[i-1], n[i-1], rx[i]))
            x_cores[i] = copy.copy(x_core)

            phi_A[i] = compute_interface_mm(phi_A[i+1], x_core, A_cores[i], Lam_cores[i], x_core, 'phi')
            phi_rhs[i] = compute_interface_mm_old(phi_rhs[i+1], x_core, None, rhs_cores[i], 'phi')

            #phi_A_old[i] = compute_interface_mm_old(phi_A_old[i+1], x_core, A_cores[i], x_core, 'phi')


        max_res = 0
        max_dx = 0

        # ALS sweep (->) with orthgonaltization and computation of psi
        for i in range(d):
            Phi1 = phi_A[i]
            Phi2 = phi_A[i+1]

            Phi1_old = phi_A_old[i]
            Phi2_old = phi_A_old[i+1]
            # Phi1: rx'1, rx1, ra1, or rx'1, ry1
            # Phi2: rx2, ra2, rx'2, or ry'2, rx2
            A_core = A_cores[i]
            Lam_core = Lam_cores[i]
            #A_core_full = A_cores_full[i]

            rhs_core = rhs_cores[i]
            # RHS - rewrite it in accordance with the new index ordering
            rhs_loc = phi_rhs[i] # rx'1, ry1
            rhs_loc = _reshape(rhs_loc, (rx[i], r_rhs[i]))
            rhs_core = _reshape(rhs_core, (r_rhs[i], n[i]*r_rhs[i+1]))
            rhs_loc = rhs_loc.dot(rhs_core)
            rhs_loc = _reshape(rhs_loc, (rx[i]*n[i], r_rhs[i+1]))
            rhs_loc = rhs_loc.dot(phi_rhs[i+1])
            rhs_loc = _reshape(rhs_loc, (rx[i]*n[i]*rx[i+1], 1))
            norm_rhs = np.linalg.norm(rhs_loc)
            # sol_prev
            sol_prev = _reshape(x_cores[i], (rx[i]*n[i]*rx[i+1], 1))

            real_tol = 0.0 #(tol / np.sqrt(d-1)) / resid_damp

            # Assembling the dense matrix of local linear systems if it is not too big
            if (rx[i]*n[i]*rx[i+1] < max_full_size):
                #      |     |    |
                # B = Phi1 - A1 - Phi2
                #      |     |    |
                # Phi1: rx1, ry1, ra1, ra1
                # Phi2: ry2, ra2, ra2, rx2

                # B = _reshape(Phi1, (rx[i]*rx[i], rA_full[i]))
                # B = B.dot(_reshape(A_core_full, (rA_full[i], n[i]*n[i]*rA_full[i+1])))
                # B = _reshape(B, (rx[i], rx[i], n[i], n[i]*rA_full[i+1]))
                # B = np.transpose(B, [0, 2, 1, 3])
                # B = _reshape(B, (rx[i]*n[i]*rx[i]*n[i], rA_full[i+1]))
                # B = B.dot(_reshape(np.transpose(Phi2, [1, 2, 3, 0]), (rA_full[i+1], rx[i+1]*rx[i+1])))
                # B = _reshape(B, (rx[i]*n[i], rx[i]*n[i], rx[i+1], rx[i+1]))
                # B = np.transpose(B, [0, 2, 1, 3]);
                # B = _reshape(B, (rx[i]*n[i]*rx[i+1], rx[i]*n[i]*rx[i+1]))

                #B_test = copy.copy(B)

                B_res = 0.
                B_res_old = 0.
                for j in range(D):
                    #B = np.transpose(Phi1[j], [0, 1, 3, 2])# ALSO FIX B IN THE NEXT LINE
                    # B = _reshape(Phi1[j], (rx[i]*rx[i]*rA[i][j], rA[i][j]))
                    # A_core_curr = np.transpose(A_core[j], [0, 2, 1, 3])
                    # A_core_curr = _reshape(A_core_curr, (rA[i][j], n[i]*l[i][j]*rA[i+1][j]))
                    # B = B.dot(A_core_curr) # -> (rx[i]*rx[i]*rA[i][j], n[i][j]*l[i][j]*rA[i+1][j])
                    # B = _reshape(B, (rx[i], rx[i], rA[i][j], n[i], l[i][j], rA[i+1][j]))#(rx[i], rx[i], n[i], n[i]*rA[i+1]))
                    # B = np.transpose(B, [0, 3, 1, 5, 2, 4]) # (rx[i], n[i][j], rx[i], rA[i+1][j], rA[i][j], l[i][j])
                    # B = _reshape(B, (rx[i]*n[i]*rx[i]*rA[i+1][j], rA[i][j]*l[i][j]))
                    # A_core_curr = _reshape(A_core[j], (rA[i][j]*l[i][j], n[i]*rA[i+1][j]))
                    # B = B.dot(A_core_curr) # -> (rx[i]*n[i][j]*rx[i]*rA[i+1][j], n[i][j]*rA[i+1][j])
                    # B = _reshape(B, (rx[i]*n[i]*rx[i], rA[i+1][j], n[i], rA[i+1][j]))
                    # B = np.transpose(B, [0, 2, 1, 3])
                    # B = _reshape(B, (rx[i]*n[i]*rx[i]*n[i], rA[i+1][j]*rA[i+1][j]))
                    # B = B.dot(_reshape(np.transpose(Phi2[j], [2, 1, 3, 0]), (rA[i+1][j]*rA[i+1][j], rx[i+1]*rx[i+1])))
                    # #USED TO BE: B = B.dot(_reshape(np.transpose(Phi2[j], [1, 2, 3, 0]), (rA[i+1][j]*rA[i+1][j], rx[i+1]*rx[i+1])))
                    # B = _reshape(B, (rx[i]*n[i], rx[i]*n[i], rx[i+1], rx[i+1]))
                    # B = np.transpose(B, [0, 2, 1, 3])
                    # B = _reshape(B, (rx[i]*n[i]*rx[i+1], rx[i]*n[i]*rx[i+1]))
                    # #B_res += B

                    # print np.linalg.norm(Phi1[j][:, :, :, 0, :] - Phi1_old[j])
                    # print np.linalg.norm(Phi2[j][:, :, 0, :, :] - Phi2_old[j])

                    B = np.tensordot(Phi1[j], A_core[j], axes=([2], [0])) # (bx, by, nu1, nu12, nu2) (nu1, l, n, mu1) -> (bx, by, nu12, nu2, l, n, mu1)
                    B = np.tensordot(B, Lam_core[j], axes=([2, 4], [0, 2])) # (bx, by, nu12, nu2, l, n, mu1) (nu12, k, l, mu12) -> (bx, by, nu2, n, mu1, k, mu12)
                    B = np.tensordot(B, A_core[j], axes=([2, 5], [0, 1])) # (bx, by, nu2, n, mu1, k, mu12) (nu2, k, m, mu2) -> (bx, by, n, mu1, mu12, m, mu2)
                    B = np.tensordot(B, Phi2[j], axes=([3, 4, 6], [3, 2, 1])) # (bx, by, n, mu2, mu12, m, mu1) (ay, mu1, mu12, mu2, ax) -> (bx, by, n, m, ay, ax)
                    B = np.transpose(B, [0, 2, 5, 1, 3, 4]) # bx, n, ax, by, m, ay
                    B = _reshape(B, (rx[i]*n[i]*rx[i+1], rx[i]*n[i]*rx[i+1]))
                    B_res += B

                    # B_old = np.tensordot(Phi1_old[j], A_core[j], axes=([2], [0])) # (bx, by, nu1, nu2) (nu1, l, n, mu1) -> (bx, by, nu2, l, n, mu1)
                    # B_old = np.tensordot(B_old, A_core[j], axes=([2, 3], [0, 1])) # (bx, by, nu2, l, n, mu1) (nu2, l, m, mu2) -> (bx, by, n, mu1, m, mu2)
                    # B_old = np.tensordot(B_old, Phi2_old[j], axes=([3, 5], [2, 1])) # (bx, by, n, mu1, m, mu2) (ay, mu1, mu2, ax) -> (bx, by, n, m, ay, ax)
                    # B_old = np.transpose(B_old, [0, 2, 5, 1, 3, 4])
                    # B_old = _reshape(B_old, (rx[i]*n[i]*rx[i+1], rx[i]*n[i]*rx[i+1]))
                    # B_res_old += B_old
                    #
                    # print 'norm', np.linalg.norm(B_old - B) / np.linalg.norm(B_old)
                #print 'B', np.linalg.norm(B_res - B_res_old) / np.linalg.norm(B_res_old)

                B = copy.copy(B_res)
                #
                # print B
                # print B_test
                # print np.linalg.norm(B - B_test)

                res_prev = np.linalg.norm(B.dot(sol_prev) - rhs_loc) / norm_rhs
                #print res_prev, np.linalg.norm(rhs_loc), norm_rhs

                if (res_prev > real_tol):
                    if not pinv:
                        sol = np.linalg.solve(B, rhs_loc)
                    else:
                        sol = np.linalg.pinv(B, rcond=pinv).dot(rhs_loc)
                    res_new = np.linalg.norm(B.dot(sol) - rhs_loc) / norm_rhs
                else:
                    sol = copy.copy(sol_prev)
                    res_new = copy.copy(res_prev)
            else:
                # Iterative solution using fast matvec operation

                res_prev = np.linalg.norm(bmat_matvec_mm(Phi1, A_core, Lam_core, Phi2, sol_prev) - rhs_loc) / norm_rhs

                if res_prev > real_tol:
                    sol = local_iter_solve_mm(Phi1, A_core, Lam_core, Phi2, rhs_loc, real_tol, sol_prev, local_restart, local_iters)
                    res_new = np.linalg.norm(bmat_matvec_mm(Phi1, A_core, Lam_core, Phi2, sol) - rhs_loc) / norm_rhs
                else:
                    sol = copy.copy(sol_prev)
                    res_new = copy.copy(res_prev)

            if (res_prev/(res_new+1e-20) < resid_damp) and (res_new > real_tol) and verb>1:
                print('--warn-- the residual damp was smaller than in the truncation\n');

            norm_dx = np.linalg.norm(sol - sol_prev) / np.linalg.norm(sol)
            max_dx = max(max_dx, norm_dx);
            max_res = max(max_res, res_prev);

            log['dx'].append(norm_dx)
            log['res'].append(res_new)
            log['res_prev'].append(res_prev)

            # QR
            sol = _reshape(sol, (rx[i]*n[i], rx[i+1]))
            u, v = np.linalg.qr(sol)
            r = u.shape[1]

            if verb == 2:
                print('=als_solve=   block {0:d}, dx: {1:3.3e}, res: {2:3.3e}'.format(i, norm_dx, res_prev))

            if  i < d-1: # left-to-right, kickrank, etc
                x_core = x_cores[i+1]
                x_core = _reshape(x_core, (rx[i+1], n[i+1]*rx[i+2]))
                v = v.dot(x_core) # size r+radd, n2, r3

                u = _reshape(u, (rx[i], n[i], r))
                v = _reshape(v, (r, n[i+1], rx[i+2]))

                # Recompute phi. Left ones, so permute them appropriately
                phi_A[i+1] = compute_interface_mm(phi_A[i], u, A_cores[i], Lam_cores[i], u, 'psi')
                phi_rhs[i+1] = compute_interface_mm_old(phi_rhs[i], u, None, rhs_cores[i], 'psi')

                #phi_A_old[i+1] = compute_interface_mm_old(phi_A_old[i], u, A_cores[i], u, 'psi')

                # print i
                # for j in range(D):
                #     print np.linalg.norm(phi_A[i][j].flatten(order='f') - phi_A_old[i][j].flatten(order='f')) / np.linalg.norm(phi_A_old[i][j].flatten(order='f'))

                # Stuff back
                rx[i+1] = r
                x_cores[i] = u
                x_cores[i+1] = v

            else:
                # Just stuff back the last core
                sol = _reshape(sol, (rx[i], n[i], rx[i+1]))
                x_cores[i] = copy.copy(sol)

        dt = t1 - time.time()
        log['time'].append(dt)
        if verb > 0:
             print('=als_solve= max_dx: {0:3.3e}, max_res: {1:3.3e}'.format(max_dx, max_res))#, sqrt(rx(1:d)'*(n.*rx(2:d+1))/sum(n))))

        if max_res < tol:
            break
    return tt.vector.from_list(x_cores)


def bmat_matvec_mm(Phi1, A, Lam, Phi2, x):

    ry1, rx1, _, _, _ = Phi1[0].shape
    rx2, _, _, _, ry2 = Phi2[0].shape
    x = _reshape(x, (rx1, -1, rx2))

    D = len(A)
    y = 0.0
    for i in range(D):
        #(ay, mu1, mu2, ax) bx, by, nu2, nu12, nu1

        yi = np.tensordot(Phi2[i], x, axes=([0], [2])) # (rx2, nu1, nu12, nu2, ry2) (rx1, m, rx2) -> (nu1, nu12, nu2, ry2, rx1, m)
        yi = np.tensordot(A[i], yi, axes=([2, 3], [5, 0])) # (mu1, l, m, nu1) (nu1, nu12, nu2, ry2, rx1, m) -> (mu1, l, nu12, nu2, ry2, rx1)
        yi = np.tensordot(Lam[i], yi, axes=([2, 3], [1, 2])) # (mu12, k, l, nu12) (mu1, l, nu12, nu2, ry2, rx1) -> (mu12, k, mu1, nu2, ry2, rx1)
        yi = np.tensordot(A[i], yi, axes=([1, 3], [1, 3])) # (mu2, k, n, nu2) (mu12, k, mu1, nu2, ry2, rx1) -> (mu2, n, mu12, mu1, ry2, rx1)
        yi = np.tensordot(Phi1[i], yi, axes=([1, 2, 3, 4], [5, 0, 2, 3])) # (ry1, rx1, mu1, mu12, mu2) (mu2, n, mu12, mu1, ry2, rx1)-> (ry1, n, ry2)
        #bx, by, nu2, nu12, nu1

        y += yi
    y = _reshape(y, (-1, 1))
    return y


def compute_interface_mm(interface_prev, x, A, Lam, y, which):

    '''
    Performs the recurrent Phi (or Psi) matrix computation
    Phi = interface_prev * (x'Ay).

    A can be empty, then only x'y is computed.

    Phi1: rx1, ry1, ra1, or rx1, ry1
    Phi2: ry2, ra2, rx2, or ry2, rx2

    which: either 'psi' or 'phi'
    '''

    rx1, n, rx2 = x.shape
    ry1, m, ry2 = y.shape

    if A is not None:
        D = len(A)
        ra1 = [A[i].shape[0] for i in range(D)]
        ra2 = [A[i].shape[-1] for i in range(D)]
        ra12 = [Lam[i].shape[-1] for i in range(D)]
        l = [A[i].shape[1] for i in range(D)]
    else:
        ra1, ra2 = 1, 1

    if which == 'psi':
        if A is not None:
            Phi_res = []
            for i in range(D):

                Phi = np.tensordot(interface_prev[i], x, axes=([0], [0])) #(ax, ay, mu1, mu12, mu2) (ax, n, bx) -> (ay, mu1, mu12, mu2, n, bx)
                Phi = np.tensordot(Phi, A[i], axes=([1, 4], [0, 2])) # (ay, mu1, mu12, mu2, n, bx) (mu1, l, n, nu1) -> (ay, mu12, mu2, bx, l, nu1)
                Phi = np.tensordot(Phi, Lam[i], axes=([1, 4], [0, 2])) # (ay, mu12, mu2, bx, l, nu1) (mu12, k, l, nu12) -> (ay, mu2, bx, nu1, k, nu12)
                Phi = np.tensordot(Phi, A[i], axes=([1, 4], [0, 1])) # (ay, mu2, bx, nu1, k, nu12) (mu2, k, m, nu2) -> (ay, bx, nu1, nu12, m, nu2)
                Phi = np.tensordot(Phi, y, axes=([0, 4], [0, 1])) # (ay, bx, nu1, nu12, m, nu2) (ay, m, by) -> (bx, nu1, nu12, nu2, by)
                Phi = np.transpose(Phi, [0, 4, 1, 2, 3]) # bx, by, nu1, nu12, nu2

                Phi_res.append(Phi)

            Phi = copy.copy(Phi_res)
        else:
            x = _reshape(x, (rx1, n*rx2))
            y = _reshape(y, (ry1*m, ry2))
            Phi = _reshape(interface_prev, (rx1, ry1*ra1))
            Phi = x.T.dot(Phi)
            Phi = _reshape(Phi, (n, rx2*ry1))
            Phi = Phi.T
            Phi = _reshape(Phi, (ra2*rx2, ry1*m))

            y = _reshape(y, (ry1*m, ry2))
            Phi = Phi.dot(y)
            Phi = _reshape(Phi, (rx2, ry2, ra2))

    elif which == 'phi':
        if A is not None:
            Phi_res = []
            for i in range(D):
                Phi1 = np.tensordot(interface_prev[i], y, axes=([0], [2])) #(by, nu1, nu12, nu2, bx) (ay, m, by) -> (nu1, nu12, nu2, bx, ay, m)
                Phi1 = np.tensordot(Phi1, A[i], axes=([0, 5], [3, 2])) # (nu1, nu12, nu2, bx, ay, m) (mu1, l, m, nu1) -> (nu12, nu2, bx, ay, mu1, l)
                Phi1 = np.tensordot(Phi1, Lam[i], axes=([0, 5], [3, 2])) # (nu12, nu2, bx, ay, mu1, l) (mu12, k, l, nu12) -> (nu2, bx, ay, mu1, mu12, k)
                Phi1 = np.tensordot(Phi1, A[i], axes=([0, 5], [3, 1])) # (nu2, bx, ay, mu1, mu12, k) (mu2, k, n, nu2) -> (bx, ay, mu1, mu12, mu2, n)
                Phi1 = np.tensordot(Phi1, x, axes=([0, 5], [2, 1])) # (bx, ay, mu1, mu12, mu2, n) (ax, n, bx) -> (ay, mu1, mu12, mu2, ax)
                Phi_res.append(Phi1)
            Phi = copy.copy(Phi_res)

        else:
            y = _reshape(y, (ry1*m, ry2))
            x = _reshape(x, (rx1, n*rx2))

            Phi = _reshape(interface_prev, (ry2, ra2*rx2))
            Phi = y.dot(Phi)
            Phi = _reshape(Phi, (ry1*ra1, n*rx2))
            x = _reshape(x, (rx1, n*rx2))
            Phi = Phi.dot(x.T)
            Phi = _reshape(Phi, (ry1, rx1))

    else:
        raise Exception('Incorrect parameter {which} name. Must be either phi or psi')

    return Phi


def als_solve_matmult_hm(A, rhs, rank, tol,
                      x0=None, nswp=1, max_full_size=5000,
                      resid_damp=2.0, verb=1, local_iters=5,
                      local_restart=30):
    '''
        A must be a MatMulTT object
    '''
    n = rhs.n
    l = A.l # Internal index
    d = rhs.d
    rA = A.r
    r_rhs = rhs.r
    D = A.D

    #rA_full = A_full.tt.r

    log = {}
    log['dx'] = []
    log['time'] = []
    log['res'] = []
    log['res_prev'] = []


    if not isinstance(rank, (list, tuple, np.ndarray)):
        rx = np.ones(d+1, dtype=np.int32)
        rx[1:d] = rank
    else:
        rx = copy.copy(rank)

    if x0 is None:
        x = tt.rand(n, r=rx)
    else:
        x = x0.copy()

    # Interfaces (to efficiently form matrices of local systems)
    phi_A = [None] * (d+1)
    phi_A[0] = [np.ones((1, 1, 1, 1, 1))]*D
    phi_A[-1] = [np.ones((1, 1, 1, 1, 1))]*D

    phi_rhs = [None] * (d+1)
    phi_rhs[0] = np.ones((1, 1))
    phi_rhs[-1] = np.ones((1, 1))

    x_cores = tt.vector.to_list(x)
    rhs_cores = tt.vector.to_list(rhs)
    A_cores = A.cores

    Lam_cores = A.cores_Lam

    #A_cores_full = tt.matrix.to_list(A_full)

    for k in range(nswp):

        t1 = time.time()

        # Orthogonalization (<-) and computation of phi
        for i in reversed(range(1, d)):
            x_core = x_cores[i]
            x_core = _reshape(x_core, (rx[i], n[i]*rx[i+1]))
            x_core, R = np.linalg.qr(x_core.T)
            x_core2 = x_cores[i-1]
            x_core2 = _reshape(x_core2, (rx[i-1]*n[i-1], rx[i]))
            x_core2 = x_core2.dot(R.T)
            rx[i] = x_core.shape[1]
            x_core = _reshape(x_core.T, (rx[i], n[i], rx[i+1]))
            x_cores[i-1] = _reshape(x_core2, (rx[i-1], n[i-1], rx[i]))
            x_cores[i] = copy.copy(x_core)

            phi_A[i] = compute_interface_mm(phi_A[i+1], x_core, A_cores[i], Lam_cores[i], x_core, 'phi')
            phi_rhs[i] = compute_interface_mm_old(phi_rhs[i+1], x_core, None, rhs_cores[i], 'phi')

        max_res = 0
        max_dx = 0

        # ALS sweep (->) with orthgonaltization and computation of psi
        for i in range(d):
            Phi1 = phi_A[i]
            Phi2 = phi_A[i+1]
            # Phi1: rx'1, rx1, ra1, or rx'1, ry1
            # Phi2: rx2, ra2, rx'2, or ry'2, rx2
            A_core = A_cores[i]
            Lam_core = Lam_cores[i]
            #A_core_full = A_cores_full[i]

            rhs_core = rhs_cores[i]
            # RHS - rewrite it in accordance with the new index ordering
            rhs_loc = phi_rhs[i] # rx'1, ry1
            rhs_loc = _reshape(rhs_loc, (rx[i], r_rhs[i]))
            rhs_core = _reshape(rhs_core, (r_rhs[i], n[i]*r_rhs[i+1]))
            rhs_loc = rhs_loc.dot(rhs_core)
            rhs_loc = _reshape(rhs_loc, (rx[i]*n[i], r_rhs[i+1]))
            rhs_loc = rhs_loc.dot(phi_rhs[i+1])
            rhs_loc = _reshape(rhs_loc, (rx[i]*n[i]*rx[i+1], 1))
            norm_rhs = np.linalg.norm(rhs_loc)
            # sol_prev
            sol_prev = _reshape(x_cores[i], (rx[i]*n[i]*rx[i+1], 1))

            real_tol = (tol / np.sqrt(d-1)) / resid_damp

            # Assembling the dense matrix of local linear systems if it is not too big
            if (rx[i]*n[i]*rx[i+1] < max_full_size):
                #      |     |    |
                # B = Phi1 - A1 - Phi2
                #      |     |    |
                # Phi1: rx1, ry1, ra1, ra1
                # Phi2: ry2, ra2, ra2, rx2

                # B = _reshape(Phi1, (rx[i]*rx[i], rA_full[i]))
                # B = B.dot(_reshape(A_core_full, (rA_full[i], n[i]*n[i]*rA_full[i+1])))
                # B = _reshape(B, (rx[i], rx[i], n[i], n[i]*rA_full[i+1]))
                # B = np.transpose(B, [0, 2, 1, 3])
                # B = _reshape(B, (rx[i]*n[i]*rx[i]*n[i], rA_full[i+1]))
                # B = B.dot(_reshape(np.transpose(Phi2, [1, 2, 3, 0]), (rA_full[i+1], rx[i+1]*rx[i+1])))
                # B = _reshape(B, (rx[i]*n[i], rx[i]*n[i], rx[i+1], rx[i+1]))
                # B = np.transpose(B, [0, 2, 1, 3]);
                # B = _reshape(B, (rx[i]*n[i]*rx[i+1], rx[i]*n[i]*rx[i+1]))

                #B_test = copy.copy(B)

                B_res = 0.
                for j in range(D):
                    #B = np.transpose(Phi1[j], [0, 1, 3, 2])# ALSO FIX B IN THE NEXT LINE
                    # B = _reshape(Phi1[j], (rx[i]*rx[i]*rA[i][j], rA[i][j]))
                    # A_core_curr = np.transpose(A_core[j], [0, 2, 1, 3])
                    # A_core_curr = _reshape(A_core_curr, (rA[i][j], n[i]*l[i][j]*rA[i+1][j]))
                    # B = B.dot(A_core_curr) # -> (rx[i]*rx[i]*rA[i][j], n[i][j]*l[i][j]*rA[i+1][j])
                    # B = _reshape(B, (rx[i], rx[i], rA[i][j], n[i], l[i][j], rA[i+1][j]))#(rx[i], rx[i], n[i], n[i]*rA[i+1]))
                    # B = np.transpose(B, [0, 3, 1, 5, 2, 4]) # (rx[i], n[i][j], rx[i], rA[i+1][j], rA[i][j], l[i][j])
                    # B = _reshape(B, (rx[i]*n[i]*rx[i]*rA[i+1][j], rA[i][j]*l[i][j]))
                    # A_core_curr = _reshape(A_core[j], (rA[i][j]*l[i][j], n[i]*rA[i+1][j]))
                    # B = B.dot(A_core_curr) # -> (rx[i]*n[i][j]*rx[i]*rA[i+1][j], n[i][j]*rA[i+1][j])
                    # B = _reshape(B, (rx[i]*n[i]*rx[i], rA[i+1][j], n[i], rA[i+1][j]))
                    # B = np.transpose(B, [0, 2, 1, 3])
                    # B = _reshape(B, (rx[i]*n[i]*rx[i]*n[i], rA[i+1][j]*rA[i+1][j]))
                    # B = B.dot(_reshape(np.transpose(Phi2[j], [2, 1, 3, 0]), (rA[i+1][j]*rA[i+1][j], rx[i+1]*rx[i+1])))
                    # #USED TO BE: B = B.dot(_reshape(np.transpose(Phi2[j], [1, 2, 3, 0]), (rA[i+1][j]*rA[i+1][j], rx[i+1]*rx[i+1])))
                    # B = _reshape(B, (rx[i]*n[i], rx[i]*n[i], rx[i+1], rx[i+1]))
                    # B = np.transpose(B, [0, 2, 1, 3])
                    # B = _reshape(B, (rx[i]*n[i]*rx[i+1], rx[i]*n[i]*rx[i+1]))
                    # #B_res += B
                    #
                    B = np.tensordot(Phi1[j], A_core[j], axes=([2], [0])) # (bx, by, nu1, nu12, nu2) (nu1, l, n, mu1) -> (bx, by, nu12, nu2, l, n, mu1)
                    B = np.tensordot(B, Lam_core[j], axes=([2, 4], [0, 2])) # (bx, by, nu12, nu2, l, n, mu1) (nu12, k, l, mu12) -> (bx, by, nu2, n, mu1, k, mu12)
                    B = np.tensordot(B, A_core[j], axes=([2, 5], [0, 1])) # (bx, by, nu2, n, mu1, k, mu12) (nu2, k, m, mu2) -> (bx, by, n, mu1, mu12, m, mu2)
                    B = np.tensordot(B, Phi2[j], axes=([3, 4, 6], [1, 2, 3])) # (bx, by, n, mu1, mu12, m, mu2) (ay, mu1, mu12, mu2, ax) -> (bx, by, n, m, ay, ax)
                    B = np.transpose(B, [0, 2, 5, 1, 3, 4]) # bx, n, ax, by, m, ay
                    B = _reshape(B, (rx[i]*n[i]*rx[i+1], rx[i]*n[i]*rx[i+1]))
                    B_res += B

                    # B = np.tensordot(Phi1[j], A_core[j], axes=([2], [0])) # (bx, by, nu1, nu2) (nu1, l, n, mu1) -> (bx, by, nu2, l, n, mu1)
                    # B = np.tensordot(B, A_core[j], axes=([2, 3], [0, 1])) # (bx, by, nu2, l, n, mu1) (nu2, l, m, mu2) -> (bx, by, n, mu1, m, mu2)
                    # B = np.tensordot(B, Phi2[j], axes=([3, 5], [2, 1])) # (bx, by, n, mu1, m, mu2) (ay, mu1, mu2, ax) -> (bx, by, n, m, ay, ax)
                    # B = np.transpose(B, [0, 2, 5, 1, 3, 4])
                    # B = _reshape(B, (rx[i]*n[i]*rx[i+1], rx[i]*n[i]*rx[i+1]))

                    B_res += B

                    #print np.linalg.norm(B1 - B)
                B = copy.copy(B_res)
                #
                # print B
                # print B_test
                # print np.linalg.norm(B - B_test)

                res_prev = np.linalg.norm(B.dot(sol_prev) - rhs_loc) / norm_rhs

                if (res_prev > real_tol):
                    sol = np.linalg.solve(B, rhs_loc)
                    res_new = np.linalg.norm(B.dot(sol) - rhs_loc) / norm_rhs
                else:
                    sol = copy.copy(sol_prev)
                    res_new = copy.copy(res_prev)
                    print 'WTF'
            else:
                # Iterative solution using fast matvec operation

                res_prev = np.linalg.norm(bmat_matvec_mm(Phi1, A_core, Phi2, sol_prev) - rhs_loc) / norm_rhs

                if res_prev > real_tol:
                    sol = local_iter_solve_mm(Phi1, A_core, Phi2, rhs_loc, real_tol, sol_prev, local_restart, local_iters)
                    res_new = np.linalg.norm(bmat_matvec_mm(Phi1, A_core, Phi2, sol) - rhs_loc) / norm_rhs
                else:
                    sol = copy.copy(sol_prev)
                    res_new = copy.copy(res_prev)

            if (res_prev/(res_new+1e-20) < resid_damp) and (res_new > real_tol) and verb>1:
                print('--warn-- the residual damp was smaller than in the truncation\n');

            norm_dx = np.linalg.norm(sol - sol_prev) / np.linalg.norm(sol)
            max_dx = max(max_dx, norm_dx);
            max_res = max(max_res, res_prev);

            log['dx'].append(norm_dx)
            log['res'].append(res_new)
            log['res_prev'].append(res_prev)

            # QR
            sol = _reshape(sol, (rx[i]*n[i], rx[i+1]))
            u, v = np.linalg.qr(sol)
            r = u.shape[1]

            if verb == 2:
                print('=als_solve=   block {0:d}, dx: {1:3.3e}, res: {2:3.3e}'.format(i, norm_dx, res_prev))

            if  i < d-1: # left-to-right, kickrank, etc
                x_core = x_cores[i+1]
                x_core = _reshape(x_core, (rx[i+1], n[i+1]*rx[i+2]))
                v = v.dot(x_core) # size r+radd, n2, r3

                u = _reshape(u, (rx[i], n[i], r))
                v = _reshape(v, (r, n[i+1], rx[i+2]))

                # Recompute phi. Left ones, so permute them appropriately
                phi_A[i+1] = compute_interface_mm(phi_A[i], u, A_cores[i], Lam_cores[i], u, 'psi')
                phi_rhs[i+1] = compute_interface_mm_old(phi_rhs[i], u, None, rhs_cores[i], 'psi')

                # Stuff back
                rx[i+1] = r
                x_cores[i] = u
                x_cores[i+1] = v
            else:
                # Just stuff back the last core
                sol = _reshape(sol, (rx[i], n[i], rx[i+1]))
                x_cores[i] = sol

        dt = t1 - time.time()
        log['time'].append(dt)
        if verb > 0:
             print('=als_solve= max_dx: {0:3.3e}, max_res: {1:3.3e}'.format(max_dx, max_res))#, sqrt(rx(1:d)'*(n.*rx(2:d+1))/sum(n))))

    return tt.vector.from_list(x_cores)


def als_solve_matmult_old(A, rhs, rank, tol,
                      x0=None, nswp=1, max_full_size=5000,
                      resid_damp=2.0, verb=1, local_iters=5,
                      local_restart=30):
    '''
        A must be a MatMulTT object
    '''
    n = rhs.n
    l = A.l # Internal index
    d = rhs.d
    rA = A.r
    r_rhs = rhs.r
    D = A.D

    #rA_full = A_full.tt.r

    log = {}
    log['dx'] = []
    log['time'] = []
    log['res'] = []
    log['res_prev'] = []


    if not isinstance(rank, (list, tuple, np.ndarray)):
        rx = np.ones(d+1, dtype=np.int32)
        rx[1:d] = rank
    else:
        rx = copy.copy(rank)

    if x0 is None:
        x = tt.rand(n, r=rx)
    else:
        x = x0.copy()

    # Interfaces (to efficiently form matrices of local systems)
    phi_A = [None] * (d+1)
    phi_A[0] = [np.ones((1, 1, 1, 1))]*D
    phi_A[-1] = [np.ones((1, 1, 1, 1))]*D

    phi_rhs = [None] * (d+1)
    phi_rhs[0] = np.ones((1, 1))
    phi_rhs[-1] = np.ones((1, 1))

    x_cores = tt.vector.to_list(x)
    rhs_cores = tt.vector.to_list(rhs)
    A_cores = A.cores

    #A_cores_full = tt.matrix.to_list(A_full)

    for k in range(nswp):

        t1 = time.time()

        # Orthogonalization (<-) and computation of phi
        for i in reversed(range(1, d)):
            x_core = x_cores[i]
            x_core = _reshape(x_core, (rx[i], n[i]*rx[i+1]))
            x_core, R = np.linalg.qr(x_core.T)
            x_core2 = x_cores[i-1]
            x_core2 = _reshape(x_core2, (rx[i-1]*n[i-1], rx[i]))
            x_core2 = x_core2.dot(R.T)
            rx[i] = x_core.shape[1]
            x_core = _reshape(x_core.T, (rx[i], n[i], rx[i+1]))
            x_cores[i-1] = _reshape(x_core2, (rx[i-1], n[i-1], rx[i]))
            x_cores[i] = copy.copy(x_core)

            phi_A[i] = compute_interface_mm_old(phi_A[i+1], x_core, A_cores[i], x_core, 'phi')
            phi_rhs[i] = compute_interface_mm_old(phi_rhs[i+1], x_core, None, rhs_cores[i], 'phi')

        max_res = 0
        max_dx = 0

        # ALS sweep (->) with orthgonaltization and computation of psi
        for i in range(d):
            Phi1 = phi_A[i]
            Phi2 = phi_A[i+1]
            # Phi1: rx'1, rx1, ra1, or rx'1, ry1
            # Phi2: rx2, ra2, rx'2, or ry'2, rx2
            A_core = A_cores[i]
            #A_core_full = A_cores_full[i]

            rhs_core = rhs_cores[i]
            # RHS - rewrite it in accordance with the new index ordering
            rhs_loc = phi_rhs[i] # rx'1, ry1
            rhs_loc = _reshape(rhs_loc, (rx[i], r_rhs[i]))
            rhs_core = _reshape(rhs_core, (r_rhs[i], n[i]*r_rhs[i+1]))
            rhs_loc = rhs_loc.dot(rhs_core)
            rhs_loc = _reshape(rhs_loc, (rx[i]*n[i], r_rhs[i+1]))
            rhs_loc = rhs_loc.dot(phi_rhs[i+1])
            rhs_loc = _reshape(rhs_loc, (rx[i]*n[i]*rx[i+1], 1))
            norm_rhs = np.linalg.norm(rhs_loc)
            # sol_prev
            sol_prev = _reshape(x_cores[i], (rx[i]*n[i]*rx[i+1], 1))

            real_tol = (tol / np.sqrt(d-1)) / resid_damp

            # Assembling the dense matrix of local linear systems if it is not too big
            if (rx[i]*n[i]*rx[i+1] < max_full_size):
                #      |     |    |
                # B = Phi1 - A1 - Phi2
                #      |     |    |
                # Phi1: rx1, ry1, ra1, ra1
                # Phi2: ry2, ra2, ra2, rx2

                # B = _reshape(Phi1, (rx[i]*rx[i], rA_full[i]))
                # B = B.dot(_reshape(A_core_full, (rA_full[i], n[i]*n[i]*rA_full[i+1])))
                # B = _reshape(B, (rx[i], rx[i], n[i], n[i]*rA_full[i+1]))
                # B = np.transpose(B, [0, 2, 1, 3])
                # B = _reshape(B, (rx[i]*n[i]*rx[i]*n[i], rA_full[i+1]))
                # B = B.dot(_reshape(np.transpose(Phi2, [1, 2, 3, 0]), (rA_full[i+1], rx[i+1]*rx[i+1])))
                # B = _reshape(B, (rx[i]*n[i], rx[i]*n[i], rx[i+1], rx[i+1]))
                # B = np.transpose(B, [0, 2, 1, 3]);
                # B = _reshape(B, (rx[i]*n[i]*rx[i+1], rx[i]*n[i]*rx[i+1]))

                #B_test = copy.copy(B)

                B_res = 0.
                for j in range(D):
                    #B = np.transpose(Phi1[j], [0, 1, 3, 2])# ALSO FIX B IN THE NEXT LINE
                    # B = _reshape(Phi1[j], (rx[i]*rx[i]*rA[i][j], rA[i][j]))
                    # A_core_curr = np.transpose(A_core[j], [0, 2, 1, 3])
                    # A_core_curr = _reshape(A_core_curr, (rA[i][j], n[i]*l[i][j]*rA[i+1][j]))
                    # B = B.dot(A_core_curr) # -> (rx[i]*rx[i]*rA[i][j], n[i][j]*l[i][j]*rA[i+1][j])
                    # B = _reshape(B, (rx[i], rx[i], rA[i][j], n[i], l[i][j], rA[i+1][j]))#(rx[i], rx[i], n[i], n[i]*rA[i+1]))
                    # B = np.transpose(B, [0, 3, 1, 5, 2, 4]) # (rx[i], n[i][j], rx[i], rA[i+1][j], rA[i][j], l[i][j])
                    # B = _reshape(B, (rx[i]*n[i]*rx[i]*rA[i+1][j], rA[i][j]*l[i][j]))
                    # A_core_curr = _reshape(A_core[j], (rA[i][j]*l[i][j], n[i]*rA[i+1][j]))
                    # B = B.dot(A_core_curr) # -> (rx[i]*n[i][j]*rx[i]*rA[i+1][j], n[i][j]*rA[i+1][j])
                    # B = _reshape(B, (rx[i]*n[i]*rx[i], rA[i+1][j], n[i], rA[i+1][j]))
                    # B = np.transpose(B, [0, 2, 1, 3])
                    # B = _reshape(B, (rx[i]*n[i]*rx[i]*n[i], rA[i+1][j]*rA[i+1][j]))
                    # B = B.dot(_reshape(np.transpose(Phi2[j], [2, 1, 3, 0]), (rA[i+1][j]*rA[i+1][j], rx[i+1]*rx[i+1])))
                    # #USED TO BE: B = B.dot(_reshape(np.transpose(Phi2[j], [1, 2, 3, 0]), (rA[i+1][j]*rA[i+1][j], rx[i+1]*rx[i+1])))
                    # B = _reshape(B, (rx[i]*n[i], rx[i]*n[i], rx[i+1], rx[i+1]))
                    # B = np.transpose(B, [0, 2, 1, 3])
                    # B = _reshape(B, (rx[i]*n[i]*rx[i+1], rx[i]*n[i]*rx[i+1]))
                    # #B_res += B

                    B = np.tensordot(Phi1[j], A_core[j], axes=([2], [0])) # (bx, by, nu1, nu2) (nu1, l, n, mu1) -> (bx, by, nu2, l, n, mu1)
                    B = np.tensordot(B, A_core[j], axes=([2, 3], [0, 1])) # (bx, by, nu2, l, n, mu1) (nu2, l, m, mu2) -> (bx, by, n, mu1, m, mu2)
                    B = np.tensordot(B, Phi2[j], axes=([3, 5], [2, 1])) # (bx, by, n, mu1, m, mu2) (ay, mu1, mu2, ax) -> (bx, by, n, m, ay, ax)
                    B = np.transpose(B, [0, 2, 5, 1, 3, 4])
                    B = _reshape(B, (rx[i]*n[i]*rx[i+1], rx[i]*n[i]*rx[i+1]))

                    B_res += B

                    #print np.linalg.norm(B1 - B)
                B = copy.copy(B_res)
                #
                # print B
                # print B_test
                # print np.linalg.norm(B - B_test)

                res_prev = np.linalg.norm(B.dot(sol_prev) - rhs_loc) / norm_rhs

                if (res_prev > real_tol):
                    sol = np.linalg.solve(B, rhs_loc)
                    res_new = np.linalg.norm(B.dot(sol) - rhs_loc) / norm_rhs
                else:
                    sol = copy.copy(sol_prev)
                    res_new = copy.copy(res_prev)
                    print 'WTF'
            else:
                # Iterative solution using fast matvec operation

                res_prev = np.linalg.norm(bmat_matvec_mm_old(Phi1, A_core, Phi2, sol_prev) - rhs_loc) / norm_rhs

                if res_prev > real_tol:
                    sol = local_iter_solve_mm_old(Phi1, A_core, Phi2, rhs_loc, real_tol, sol_prev, local_restart, local_iters)
                    res_new = np.linalg.norm(bmat_matvec_mm_old(Phi1, A_core, Phi2, sol) - rhs_loc) / norm_rhs
                else:
                    sol = copy.copy(sol_prev)
                    res_new = copy.copy(res_prev)

            if (res_prev/(res_new+1e-20) < resid_damp) and (res_new > real_tol) and verb>1:
                print('--warn-- the residual damp was smaller than in the truncation\n');

            norm_dx = np.linalg.norm(sol - sol_prev) / np.linalg.norm(sol)
            max_dx = max(max_dx, norm_dx);
            max_res = max(max_res, res_prev);

            log['dx'].append(norm_dx)
            log['res'].append(res_new)
            log['res_prev'].append(res_prev)

            # QR
            sol = _reshape(sol, (rx[i]*n[i], rx[i+1]))
            u, v = np.linalg.qr(sol)
            r = u.shape[1]

            if verb == 2:
                print('=als_solve=   block {0:d}, dx: {1:3.3e}, res: {2:3.3e}'.format(i, norm_dx, res_prev))

            if  i < d-1: # left-to-right, kickrank, etc
                x_core = x_cores[i+1]
                x_core = _reshape(x_core, (rx[i+1], n[i+1]*rx[i+2]))
                v = v.dot(x_core) # size r+radd, n2, r3

                u = _reshape(u, (rx[i], n[i], r))
                v = _reshape(v, (r, n[i+1], rx[i+2]))

                # Recompute phi. Left ones, so permute them appropriately
                phi_A[i+1] = compute_interface_mm_old(phi_A[i], u, A_cores[i], u, 'psi')
                phi_rhs[i+1] = compute_interface_mm_old(phi_rhs[i], u, None, rhs_cores[i], 'psi')

                # Stuff back
                rx[i+1] = r
                x_cores[i] = u
                x_cores[i+1] = v
            else:
                # Just stuff back the last core
                sol = _reshape(sol, (rx[i], n[i], rx[i+1]))
                x_cores[i] = sol

        dt = t1 - time.time()
        log['time'].append(dt)
        if verb > 0:
             print('=als_solve= max_dx: {0:3.3e}, max_res: {1:3.3e}'.format(max_dx, max_res))#, sqrt(rx(1:d)'*(n.*rx(2:d+1))/sum(n))))

    return tt.vector.from_list(x_cores)


def compute_interface_mm_old(interface_prev, x, A, y, which):

    '''
    Performs the recurrent Phi (or Psi) matrix computation
    Phi = interface_prev * (x'Ay).

    A can be empty, then only x'y is computed.

    Phi1: rx1, ry1, ra1, or rx1, ry1
    Phi2: ry2, ra2, rx2, or ry2, rx2

    which: either 'psi' or 'phi'
    '''

    rx1, n, rx2 = x.shape
    ry1, m, ry2 = y.shape

    if A is not None:
        D = len(A)
        ra1 = [A[i].shape[0] for i in range(D)]
        ra2 = [A[i].shape[-1] for i in range(D)]
        l = [A[i].shape[1] for i in range(D)]
    else:
        ra1, ra2 = 1, 1

    if which == 'psi':
        # x2 = _reshape(x, (rx1, n*rx2))
        # y2 = _reshape(y, (ry1*m, ry2))
        # x1 = _reshape(x2, (rx1, n, rx2))
        # y1 = _reshape(y2, (ry1, m, ry2))
        #print 'wtf', np.linalg.norm(x1 - x)
        if A is not None:
            Phi_res = []
            for i in range(D):
                # Phi = _reshape(interface_prev[i], (rx1, ry1*ra1[i]**2))
                # Phi = x.T.dot(Phi) # (n*rx2, rx1) @ (rx1, ry1*ra1[i]**2) -> (n*rx2, ry1*ra1[i]**2)
                # Phi = _reshape(Phi, (n*rx2*ry1*ra1[i], ra1[i]))
                # Phi = Phi.T
                # Phi = _reshape(Phi, (ra1[i]*n, rx2*ry1*ra1[i]))
                # A_curr = _reshape(np.transpose(A[i], [0, 2, 1, 3]), (ra1[i]*n, l[i]*ra2[i]))
                # Phi = A_curr.T.dot(Phi) # (l*ra2[i], ra1[i]*n) @ (ra1[i]*n, rx2*ry1*ra1[i]) -> (l*ra2[i], rx2*ry1*ra1[i])
                # Phi = _reshape(Phi, (l[i], ra2[i]*rx2*ry1*ra1[i]))
                # Phi = Phi.T
                # Phi = _reshape(Phi, (ra2[i]*rx2*ry1, ra1[i]*l[i]))
                # A_curr = _reshape(A[i], (ra1[i]*l[i], m*ra2[i]))
                # Phi = Phi.dot(A_curr) # -> (ra2[i]*rx2*ry1, m*ra2[i])
                # Phi = _reshape(Phi, (ra2[i]*rx2, ry1*m*ra2[i]))
                # Phi = Phi.T
                # Phi = _reshape(Phi, (ry1*m, ra2[i]**2*rx2)) #ra2[i]**2 must come in the proper way
                # Phi = Phi.T
                #
                # Phi = Phi.dot(y) # -> (ra2[i]**2*rx2, ry2)
                #
                # Phi = _reshape(Phi, (ra2[i]**2, rx2*ry2))
                # Phi = Phi.T
                # Phi = _reshape(Phi, (rx2, ry2, ra2[i], ra2[i]))
                #TEST
                #Phi = np.transpose(Phi, [0, 1, 3, 2])
                #Phi_res.append(Phi)

                Phi = np.tensordot(interface_prev[i], x, axes=([0], [0])) #(ax, ay, mu1, mu2) (ax, n, bx) -> (ay, mu1, mu2, n, bx)
                Phi = np.tensordot(Phi, A[i], axes=([1, 3], [0, 2])) # (ay, mu1, mu2, n, bx) (mu1, l, n, nu1) -> (ay, mu2, bx, l, nu1)
                Phi = np.tensordot(Phi, A[i], axes=([1, 3], [0, 1])) # (ay, mu2, bx, l, nu1) (mu2, l, m, nu2) -> (ay, bx, nu1, m, nu2)
                Phi = np.tensordot(Phi, y, axes=([0, 3], [0, 1])) # (ay, bx, nu1, m, nu2) (ay, m, by) -> (bx, nu1, nu2, by)
                Phi = np.transpose(Phi, [0, 3, 1, 2]) # bx, by, nu1, nu2

                Phi_res.append(Phi)

                #print np.linalg.norm(Phi1 - Phi) / np.linalg.norm(Phi)

                # # A_tmp = np.transpose(A[i], [0, 2, 1, 3])
                # # Phi = np.tensordot(x, interface_prev[i], axes=([0], [0])) #(ax, n, bx) (ax, ay, mu1, mu2) -> (n, bx, ay, mu1, mu2)
                # # Phi = np.tensordot(Phi, A_tmp, axes=([0, 3], [1, 0])) # (n, bx, ay, mu1, mu2) (mu1, n, l, nu1) -> (bx, ay, mu2, l, nu1)
                # # Phi = np.tensordot(Phi, A[i], axes=([2, 3], [0, 1])) # (bx, ay, mu2, l, nu1) (mu2, l, m, nu2) -> (bx, ay, nu1, m, nu2)
                # # Phi = np.tensordot(Phi, y, axes=([1, 3], [0, 1])) # (bx, ay, nu1, m, nu2) (ay, m, by) -> (bx, nu1, nu2, by)
                # #Phi = np.transpose(Phi, [0, 3, 1, 2])
                #

                #print np.linalg.norm(Phi1 - Phi_res[-1])

                #(a1, a2, mu1, mu2) (mu1, l, m, nu1)
            Phi = copy.copy(Phi_res)
        else:
            x = _reshape(x, (rx1, n*rx2))
            y = _reshape(y, (ry1*m, ry2))
            Phi = _reshape(interface_prev, (rx1, ry1*ra1))
            Phi = x.T.dot(Phi)
            Phi = _reshape(Phi, (n, rx2*ry1))
            Phi = Phi.T
            Phi = _reshape(Phi, (ra2*rx2, ry1*m))

            y = _reshape(y, (ry1*m, ry2))
            Phi = Phi.dot(y)
            Phi = _reshape(Phi, (rx2, ry2, ra2))

    elif which == 'phi':
        # y = _reshape(y, (ry1*m, ry2))
        # y1 = _reshape(y, (ry1, m, ry2))
        # x1 = _reshape(x, (rx1, n, rx2))
        if A is not None:
            Phi_res = []
            for i in range(D):
                # Phi = _reshape(interface_prev[i], (ry2, ra2[i]**2*rx2))
                # Phi = y.dot(Phi) # -> (ry1*m, ra2[i]**2*rx2)
                # Phi = _reshape(Phi, (ry1, m*ra2[i]**2*rx2))
                # Phi = Phi.T
                # Phi = _reshape(Phi, (m*ra2[i], ra2[i]*rx2*ry1))
                # A_curr = _reshape(A[i], (ra1[i]*l[i], m*ra2[i]))
                # Phi = A_curr.dot(Phi) # -> (ra1[i]*l, ra2[i]*rx2*ry1)
                # Phi = _reshape(Phi, (ra1[i]*l[i]*ra2[i], rx2*ry1))
                # Phi = Phi.T
                # Phi = _reshape(Phi, (rx2*ry1*ra1[i], l[i]*ra2[i]))
                # A_curr = _reshape(np.transpose(A[i], [0, 2, 1, 3]), (ra1[i]*n, l[i]*ra2[i]))
                # Phi = A_curr.dot(Phi.T) # -> (ra1[i]*n, rx2*ry1*ra1[i])
                # Phi = _reshape(Phi, (ra1[i], n*rx2*ry1*ra1[i]))
                # Phi = _reshape(Phi.T, (n*rx2, ry1*ra1[i]**2))
                # Phi = Phi.T
                # x = _reshape(x, (rx1, n*rx2))
                # Phi = Phi.dot(x.T) # -> (ry1*ra1[i]**2, rx1)
                # Phi = _reshape(Phi, (ry1, ra1[i], ra1[i], rx1))
                #Phi_res.append(Phi)

                Phi1 = np.tensordot(interface_prev[i], y, axes=([0], [2])) #(by, nu1, nu2, bx) (ay, m, by) -> (nu1, nu2, bx, ay, m)
                Phi1 = np.tensordot(Phi1, A[i], axes=([0, 4], [3, 2])) # (nu1, nu2, bx, ay, m) (mu1, l, m, nu1) -> (nu2, bx, ay, mu1, l)
                Phi1 = np.tensordot(Phi1, A[i], axes=([0, 4], [3, 1])) # (nu2, bx, ay, mu1, l) (mu2, l, n, nu2) -> (bx, ay, mu1, mu2, n)
                Phi1 = np.tensordot(Phi1, x, axes=([0, 4], [2, 1])) # (bx, ay, mu1, mu2, n) (ax, n, bx) -> (ay, mu1, mu2, ax)
                Phi_res.append(Phi1)
                #Phi1 = np.transpose(Phi1, [3, 1, 2, 0])

                #print np.linalg.norm(Phi1 - Phi)

            Phi = copy.copy(Phi_res)

        else:
            y = _reshape(y, (ry1*m, ry2))
            x = _reshape(x, (rx1, n*rx2))

            Phi = _reshape(interface_prev, (ry2, ra2*rx2))
            Phi = y.dot(Phi)
            Phi = _reshape(Phi, (ry1*ra1, n*rx2))
            x = _reshape(x, (rx1, n*rx2))
            Phi = Phi.dot(x.T)
            Phi = _reshape(Phi, (ry1, rx1))

    else:
        raise Exception('Incorrect parameter {which} name. Must be either phi or psi')

    return Phi


def als_solve(A, rhs, rank, tol,
              x0=None, nswp=1, max_full_size=5000,
              resid_damp=2.0, verb=1, local_iters=5,
              local_restart=30):

    n = A.n
    d = A.tt.d
    rA = A.tt.r
    r_rhs = rhs.r

    log = {}
    log['dx'] = []
    log['time'] = []
    log['res'] = []
    log['res_prev'] = []


    if not isinstance(rank, (list, tuple, np.ndarray)):
        rx = np.ones(d+1, dtype=np.int32)
        rx[1:d] = rank
    else:
        rx = copy.copy(rank)

    if x0 is None:
        x = tt.rand(n, r=rx)
    else:
        x = x0.copy()

    # Interfaces (to efficiently form matrices of local systems)
    phi_A = [None] * (d+1)
    phi_A[0] = np.ones((1, 1, 1))
    phi_A[-1] = np.ones((1, 1, 1))

    phi_rhs = [None] * (d+1)
    phi_rhs[0] = np.ones((1, 1))
    phi_rhs[-1] = np.ones((1, 1))

    x_cores = tt.vector.to_list(x)
    rhs_cores = tt.vector.to_list(rhs)
    A_cores = tt.matrix.to_list(A)

    for k in range(nswp):

        t1 = time.time()

        # Orthogonalization (<-) and computation of phi
        for i in reversed(range(1, d)):
            x_core = x_cores[i]
            x_core = _reshape(x_core, (rx[i], n[i]*rx[i+1]))
            x_core, R = np.linalg.qr(x_core.T)
            x_core2 = x_cores[i-1]
            x_core2 = _reshape(x_core2, (rx[i-1]*n[i-1], rx[i]))
            x_core2 = x_core2.dot(R.T)
            rx[i] = x_core.shape[1]
            x_core = _reshape(x_core.T, (rx[i], n[i], rx[i+1]))
            x_cores[i-1] = _reshape(x_core2, (rx[i-1], n[i-1], rx[i]))
            x_cores[i] = copy.copy(x_core)

            phi_A[i] = compute_interface(phi_A[i+1], x_core, A_cores[i], x_core, 'phi')
            phi_rhs[i] = compute_interface(phi_rhs[i+1], x_core, None, rhs_cores[i], 'phi')

        max_res = 0
        max_dx = 0

        # ALS sweep (->) with orthgonaltization (->) and computation of psi
        for i in range(d):
            Phi1 = phi_A[i]
            Phi2 = phi_A[i+1]
            # Phi1: rx'1, rx1, ra1, or rx'1, ry1
            # Phi2: rx2, ra2, rx'2, or ry'2, rx2
            A_core = A_cores[i]
            rhs_core = rhs_cores[i];
            # RHS - rewrite it in accordance with new index ordering
            rhs_loc = phi_rhs[i] # rx'1, ry1
            rhs_loc = _reshape(rhs_loc, (rx[i], r_rhs[i]))
            rhs_core = _reshape(rhs_core, (r_rhs[i], n[i]*r_rhs[i+1]))
            rhs_loc = rhs_loc.dot(rhs_core)
            rhs_loc = _reshape(rhs_loc, (rx[i]*n[i], r_rhs[i+1]))
            rhs_loc = rhs_loc.dot(phi_rhs[i+1])
            rhs_loc = _reshape(rhs_loc, (rx[i]*n[i]*rx[i+1], 1))
            norm_rhs = np.linalg.norm(rhs_loc)
            # sol_prev
            sol_prev = _reshape(x_cores[i], (rx[i]*n[i]*rx[i+1], 1))

            real_tol = (tol / np.sqrt(d-1)) / resid_damp

            # Assembling the dense matrix of local linear systems if it is not too big
            if (rx[i]*n[i]*rx[i+1] < max_full_size):
                #      |     |    |
                # B = Phi1 - A1 - Phi2
                #      |     |    |
                B = _reshape(Phi1, (rx[i]*rx[i], rA[i]))
                B = B.dot(_reshape(A_core, (rA[i], n[i]*n[i]*rA[i+1])))
                B = _reshape(B, (rx[i], rx[i], n[i], n[i]*rA[i+1]))
                B = np.transpose(B, [0, 2, 1, 3])
                B = _reshape(B, (rx[i]*n[i]*rx[i]*n[i], rA[i+1]))
                B = B.dot(_reshape(np.transpose(Phi2, [1, 2, 0]), (rA[i+1], rx[i+1]*rx[i+1])))
                B = _reshape(B, (rx[i]*n[i], rx[i]*n[i], rx[i+1], rx[i+1]))
                B = np.transpose(B, [0, 2, 1, 3]);
                B = _reshape(B, (rx[i]*n[i]*rx[i+1], rx[i]*n[i]*rx[i+1]))

                print('swp', k, 'i', i, 'cond B', np.linalg.cond(B))

                # Cool, but slow code for reasons unknown
                # B_new = np.einsum('abc,cijd,mdn->ainbjm', Phi1, A_core, Phi2)
                # B_new = _reshape(B_new, (rx[i]*n[i]*rx[i+1], rx[i]*n[i]*rx[i+1]))
                # assert np.linalg.norm(B_new - B) < 1e-10

                res_prev = np.linalg.norm(B.dot(sol_prev) - rhs_loc) / norm_rhs

                if (res_prev > real_tol):
                    sol = np.linalg.solve(B, rhs_loc)
                    res_new = np.linalg.norm(B.dot(sol) - rhs_loc) / norm_rhs
                else:
                    sol = copy.copy(sol_prev)
                    res_new = copy.copy(res_prev)
            else:
                # Iterative solution using fast matvec operation

                res_prev = np.linalg.norm(bmat_matvec(Phi1, A_core, Phi2, sol_prev) - rhs_loc) / norm_rhs

                if res_prev > real_tol:
                    sol = local_iter_solve(Phi1, A_core, Phi2, rhs_loc, real_tol, sol_prev, local_restart, local_iters)
                    res_new = np.linalg.norm(bmat_matvec(Phi1, A_core, Phi2, sol) - rhs_loc) / norm_rhs
                else:
                    sol = copy.copy(sol_prev)
                    res_new = copy.copy(res_prev)

            if (res_prev/res_new < resid_damp) and (res_new > real_tol) and verb>1:
                print('--warn-- the residual damp was smaller than in the truncation\n');

            norm_dx = np.linalg.norm(sol - sol_prev) / np.linalg.norm(sol)
            max_dx = max(max_dx, norm_dx);
            max_res = max(max_res, res_prev);

            log['dx'].append(norm_dx)
            log['res'].append(res_new)
            log['res_prev'].append(res_prev)

            # QR
            sol = _reshape(sol, (rx[i]*n[i], rx[i+1]))
            u, v = np.linalg.qr(sol)
            r = u.shape[1]

            if verb == 2:
                print('=als_solve=   block {0:d}, dx: {1:3.3e}, res: {2:3.3e}'.format(i, norm_dx, res_prev))

            if  i < d-1: # left-to-right, kickrank, etc
                x_core = x_cores[i+1]
                x_core = _reshape(x_core, (rx[i+1], n[i+1]*rx[i+2]))
                v = v.dot(x_core) # size r+radd, n2, r3

                u = _reshape(u, (rx[i], n[i], r))
                v = _reshape(v, (r, n[i+1], rx[i+2]))

                # Recompute phi. Left ones, so permute them appropriately
                phi_A[i+1] = compute_interface(phi_A[i], u, A_cores[i], u, 'psi')
                phi_rhs[i+1] = compute_interface(phi_rhs[i], u, None, rhs_cores[i], 'psi')

                # Stuff back
                rx[i+1] = r
                x_cores[i] = u
                x_cores[i+1] = v
            else:
                # Just stuff back the last core
                sol = _reshape(sol, (rx[i], n[i], rx[i+1]))
                x_cores[i] = sol

        dt = t1 - time.time()
        log['time'].append(dt)
        if verb > 0:
             print('=als_solve= max_dx: {0:3.3e}, max_res: {1:3.3e}'.format(max_dx, max_res))#, sqrt(rx(1:d)'*(n.*rx(2:d+1))/sum(n))))

    return tt.vector.from_list(x_cores)


def compute_interface(interface_prev, x, A, y, which):

    '''
    Performs the recurrent Phi (or Psi) matrix computation
    Phi = interface_prev * (x'Ay).

    A can be empty, then only x'y is computed.

    Phi1: rx1, ry1, ra1, or rx1, ry1
    Phi2: ry2, ra2, rx2, or ry2, rx2

    which: either 'psi' or 'phi'
    '''

    rx1, n, rx2 = x.shape
    ry1, m, ry2 = y.shape

    if A is not None:
        ra1, ra2 = A.shape[0], A.shape[-1]
    else:
        ra1, ra2 = 1, 1

    if which == 'psi':
        x = _reshape(x, (rx1, n*rx2))
        Phi = _reshape(interface_prev, (rx1, ry1*ra1))
        Phi = x.T.dot(Phi)
        if A is not None:
            Phi = _reshape(Phi, (n*rx2*ry1, ra1))
            Phi = Phi.T
            Phi = _reshape(Phi, (ra1*n, rx2*ry1))
            A = _reshape(A, (ra1*n, m*ra2))
            Phi = A.T.dot(Phi)
            Phi = _reshape(Phi, (m, ra2*rx2*ry1))
        else:
            Phi = _reshape(Phi, (n, rx2*ry1))
        Phi = Phi.T
        Phi = _reshape(Phi, (ra2*rx2, ry1*m))

        y = _reshape(y, (ry1*m, ry2))
        Phi = Phi.dot(y)
        if A is not None:
            Phi = _reshape(Phi, (ra2, rx2*ry2))
            Phi = Phi.T
        Phi = _reshape(Phi, (rx2, ry2, ra2))

    elif which == 'phi':
        y = _reshape(y, (ry1*m, ry2))
        Phi = _reshape(interface_prev, (ry2, ra2*rx2))
        Phi = y.dot(Phi)
        if A is not None:
            Phi = _reshape(Phi, (ry1, m*ra2*rx2))
            Phi = Phi.T
            Phi = _reshape(Phi, (m*ra2, rx2*ry1))
            A = _reshape(A, (ra1*n, m*ra2))
            Phi = A.dot(Phi)
            Phi = _reshape(Phi, (ra1*n*rx2, ry1))
            Phi = Phi.T

        Phi = _reshape(Phi, (ry1*ra1, n*rx2))
        x = _reshape(x, (rx1, n*rx2))
        Phi = Phi.dot(x.T.conj())
        if A is not None:
            Phi = _reshape(Phi, (ry1, ra1, rx1))
        else:
            Phi = _reshape(Phi, (ry1, rx1))
    else:
        raise Exception('Incorrect parameter {which} name. Must be either phi or psi')

    return Phi


def bmat_matvec_mm_old(Phi1, A, Phi2, x):

    ry1, rx1, _, _ = Phi1[0].shape
    rx2, _, _, ry2 = Phi2[0].shape
    x = _reshape(x, (rx1, -1, rx2))

    D = len(A)
    # ra1 = [A[i].shape[0] for i in range(D)]
    # ra2 = [A[i].shape[-1] for i in range(D)]
    # l = [A[i].shape[1] for i in range(D)]

    #n = A.shape[1]
    #m = A.shape[2]

    # for i in range(D):
    #     y = _reshape(x, (rx1*m, rx2))
    #     Phi2 = _reshape(Phi2[i], (rx2, ra2*ry2))
    #     y = y.dot(Phi2)
    #     y = _reshape(y, (rx1, m*ra2*ry2))
    #     y = y.T
    #     y = _reshape(y, (m*ra2, ry2*rx1))
    #     A = _reshape(A, (ra1*n, m*ra2))
    #     y = A.dot(y)
    #     y = _reshape(y, (ra1*n*ry2, rx1))
    #     y = y.T
    #     y = _reshape(y, (rx1*ra1, n*ry2))
    #     Phi1 = _reshape(Phi1, (ry1, rx1*ra1))
    #     y = Phi1.dot(y)
    #     y = _reshape(y, (ry1*n*ry2, 1))


    y = 0.0
    for i in range(D):

        yi = np.tensordot(Phi2[i], x, axes=([0], [2])) # (ay, nu1, nu2, ax) (by, m, ay) -> (nu1, nu2, ax, by, m)
        yi = np.tensordot(A[i], yi, axes=([2, 3], [4, 0])) # (mu1, l, m, nu1) (nu1, nu2, ax, by, m) -> (mu1, l, nu2, ax, by)
        yi = np.tensordot(A[i], yi, axes=([1, 3], [1, 2])) # (mu2, l, n, nu2) (mu1, l, nu2, ax, by) -> (mu2, n, mu1, ax, by)
        yi = np.tensordot(Phi1[i], yi, axes=([1, 2, 3], [4, 0, 2])) # (bx, by, mu1, mu2) (mu2, n, mu1, ax, by) -> (bx, n, ax)

        y += yi
    y = _reshape(y, (-1, 1))




    return y


def bmat_matvec(Phi1, A, Phi2, x):

    ry1, rx1, ra1 = Phi1.shape
    rx2, ra2, ry2 = Phi2.shape

    n = A.shape[1]
    m = A.shape[2]

    y = _reshape(x, (rx1*m, rx2))
    Phi2 = _reshape(Phi2, (rx2, ra2*ry2))
    y = y.dot(Phi2)
    y = _reshape(y, (rx1, m*ra2*ry2))
    y = y.T
    y = _reshape(y, (m*ra2, ry2*rx1))
    A = _reshape(A, (ra1*n, m*ra2))
    y = A.dot(y)
    y = _reshape(y, (ra1*n*ry2, rx1))
    y = y.T
    y = _reshape(y, (rx1*ra1, n*ry2))
    Phi1 = _reshape(Phi1, (ry1, rx1*ra1))
    y = Phi1.dot(y)
    y = _reshape(y, (ry1*n*ry2, 1))

    return y


#def bmat_matvec_mm_old(Phi1, A, Phi2, x):

    ry1, rx1, _, _ = Phi1[0].shape
    rx2, _, _, ry2 = Phi2[0].shape
    x = _reshape(x, (rx1, -1, rx2))

    D = len(A)
    # ra1 = [A[i].shape[0] for i in range(D)]
    # ra2 = [A[i].shape[-1] for i in range(D)]
    # l = [A[i].shape[1] for i in range(D)]

    #n = A.shape[1]
    #m = A.shape[2]

    # for i in range(D):
    #     y = _reshape(x, (rx1*m, rx2))
    #     Phi2 = _reshape(Phi2[i], (rx2, ra2*ry2))
    #     y = y.dot(Phi2)
    #     y = _reshape(y, (rx1, m*ra2*ry2))
    #     y = y.T
    #     y = _reshape(y, (m*ra2, ry2*rx1))
    #     A = _reshape(A, (ra1*n, m*ra2))
    #     y = A.dot(y)
    #     y = _reshape(y, (ra1*n*ry2, rx1))
    #     y = y.T
    #     y = _reshape(y, (rx1*ra1, n*ry2))
    #     Phi1 = _reshape(Phi1, (ry1, rx1*ra1))
    #     y = Phi1.dot(y)
    #     y = _reshape(y, (ry1*n*ry2, 1))
    y = 0.0
    for i in range(D):
        # yi = np.tensordot(Phi2[i], x, axes=([0], [2])) # (rx2, nu2, nu1, ry2) (rx1, m, rx2) -> (nu2, nu1, ry2, rx1, m)
        # yi = np.tensordot(A[i], yi, axes=([2, 3], [4, 1])) # (mu1, l, m, nu1) (nu2, nu1, ry2, rx1, m) -> (mu1, l, nu2, ry2, rx1)
        # yi = np.tensordot(A[i], yi, axes=([1, 3], [1, 2])) # (mu2, l, n, nu2) (mu1, l, nu2, ry2, rx1) -> (mu2, n, mu1, ry2, rx1)
        # yi = np.tensordot(Phi1[i], yi, axes=([1, 2, 3], [4, 2, 0])) # (ry1, rx1, mu1, mu2) (mu2, n, mu1, ry2, rx1) -> (ry1, n, ry2)

        yi = np.tensordot(Phi2[i], x, axes=([0], [2])) # (rx2, nu1, nu2, ry2) (rx1, m, rx2) -> (nu1, nu2, ry2, rx1, m)
        yi = np.tensordot(A[i], yi, axes=([2, 3], [4, 0])) # (mu1, l, m, nu1) (nu1, nu2, ry2, rx1, m) -> (mu1, l, nu2, ry2, rx1)
        yi = np.tensordot(A[i], yi, axes=([1, 3], [1, 2])) # (mu2, l, n, nu2) (mu1, l, nu2, ry2, rx1) -> (mu2, n, mu1, ry2, rx1)
        yi = np.tensordot(Phi1[i], yi, axes=([1, 2, 3], [4, 2, 0])) # (ry1, rx1, mu1, mu2) (mu2, n, mu1, ry2, rx1) -> (ry1, n, ry2)

        y += yi
    y = _reshape(y, (-1, 1))
    return y


def local_iter_solve_mm(Phi1, A, Lam, Phi2, y, tol, x0, local_restart, local_iters, prec=None):
    # Iterative solution of local ALS problems

    dy = bmat_matvec_mm(Phi1, A, Lam, Phi2, x0)
    dy = y - dy
    norm_dy = np.linalg.norm(dy)
    real_tol = tol / norm_dy

    if real_tol < 1.0:
        #ry1, rx1, ra1 = Phi1.shape
        #rx2, ra2, ry2 = Phi2.shape
        #n = A.shape[1]
        mv = sp.linalg.LinearOperator((dy.shape[0], dy.shape[0]), lambda x: bmat_matvec_mm(Phi1, A, Lam, Phi2, x))
        dx, info = sp.linalg.gmres(mv, dy[:,0], restart=local_restart, tol=real_tol, maxiter=local_iters)
        dx = dx.reshape(-1, 1, order='f')
        x = x0 + dx
    else:
        x = x0
    return x


def local_iter_solve_mm_old(Phi1, A, Phi2, y, tol, x0, local_restart, local_iters, prec=None):
    # Iterative solution of local ALS problems

    dy = bmat_matvec_mm_old(Phi1, A, Phi2, x0)
    dy = y - dy
    norm_dy = np.linalg.norm(dy)
    real_tol = tol / norm_dy

    if real_tol < 1.0:
        #ry1, rx1, ra1 = Phi1.shape
        #rx2, ra2, ry2 = Phi2.shape
        #n = A.shape[1]
        mv = sp.linalg.LinearOperator((dy.shape[0], dy.shape[0]), lambda x: bmat_matvec_mm_old(Phi1, A, Phi2, x))
        dx, info = sp.linalg.gmres(mv, dy[:,0], restart=local_restart, tol=real_tol, maxiter=local_iters)
        dx = dx.reshape(-1, 1, order='f')
        x = x0 + dx
    else:
        x = x0
    return x


def local_iter_solve(Phi1, A, Phi2, y, tol, x0, local_restart, local_iters, prec=None):
    # Iterative solution of local ALS problems

    dy = bmat_matvec(Phi1, A, Phi2, x0)
    dy = y - dy
    norm_dy = np.linalg.norm(dy)
    real_tol = tol / norm_dy

    if real_tol < 1.0:
        #ry1, rx1, ra1 = Phi1.shape
        #rx2, ra2, ry2 = Phi2.shape
        #n = A.shape[1]
        mv = sp.linalg.LinearOperator((dy.shape[0], dy.shape[0]), lambda x: bmat_matvec(Phi1, A, Phi2, x))
        dx, info = sp.linalg.gmres(mv, dy[:,0], restart=local_restart, tol=real_tol, maxiter=local_iters)
        dx = dx.reshape(-1, 1, order='f')
        x = x0 + dx
    else:
        x = x0
    return x


def dot_mat(tt_1, tt_2, matrix=None):
    ndims = tt_1.d
    crs_1 = tt.tensor.to_list(tt_1)
    crs_2 = tt.tensor.to_list(tt_2)
    curr_core_1 = crs_1[0]
    curr_core_2 = crs_2[0]
    if matrix is None:
        einsum_str = 'aib,cid->bd'
        res = np.einsum(einsum_str, curr_core_1, curr_core_2)
        for core_idx in range(1, ndims):
            curr_core_1 = crs_1[core_idx]
            curr_core_2 = crs_2[core_idx]
            einsum_str = 'ac,aib,cid->bd'
            res = np.einsum('ac,aib->cib', res, curr_core_1)
            res = np.einsum('cib,cid->bd', res, curr_core_2)
    else:
        crs_mat = tt.matrix.to_list(matrix)
        curr_matrix_core = crs_mat[0]
        # aib,cijd,ejf->bdf
        res = np.tensordot(curr_core_1, curr_matrix_core, axes=([1], [1])) #aib,cijd->abcjd
        res = np.tensordot(res, curr_core_2, axes=([3], [1])) #abcjd,ejf->abcdef
        res = res[0, :, 0, :, 0, :] #abcdef->bdf
        for core_idx in range(1, ndims):
            curr_core_1 = crs_1[core_idx]
            curr_core_2 = crs_2[core_idx]
            curr_matrix_core = crs_mat[core_idx]
            # ace,aib,cijd,ejf->bdf
            res = np.tensordot(res, curr_core_1, axes=([0], [0])) #ace,aib->ceib
            res = np.tensordot(res, curr_matrix_core, axes=([0, 2], [0, 1])) #ceib,cijd->ebjd
            res = np.tensordot(res, curr_core_2, axes=([0, 2], [0, 1])) #ebjd,ejf->bdf

    return np.squeeze(res)


def amen_mm(A, B, tol, x0=None, nswp=20, kickrank=4, verb=1, init_qr=True):
    # Approximate the matrix-by-matrix via the AMEn iteration
    #    [X]=amen_mv(A, Y, tol, varargin)
    #    Attempts to approximate the X = A*Y with accuracy TOL using the
    #    AMEn+ALS iteration. A is a n x m matrix, Y is a m x k matrix.
    #    Matrices A,Y can be given in tt_matrix format, the output is tt_matrix.
    #    Y can be tt_tensor, it's considered as column, the output is tt_tensor.
    #    A and Y can be a {d,R} cell array. However, X is always a "single" TT
    #    (no tensor chain), since that's how ALS works. Generally, X has the
    #    same form as Y, except that it's always {d,1} in essense. X and Y can't
    #    be sparse (SVD will destroy it anyways), but A can be.
    #
    #    Options are provided in form
    #    'PropertyName1',PropertyValue1,'PropertyName2',PropertyValue2 and so
    #    on. The parameters are set to default (in brackets in the following)
    #    The list of option names and default values are:
    #        o x0 - initial approximation to AY [rand with ranks of Y(:,1)]
    #        o nswp - maximal number of sweeps [20]
    #        o verb - verbosity level, 0-silent, 1-sweep info, 2-block info [1]
    #        o kickrank - compression rank of the error,
    #                     i.e. enrichment size [4]
    #
    #
    # ********
    #    For description of adaptive ALS please see
    #    Sergey V. Dolgov, Dmitry V. Savostyanov,
    #    Alternating minimal energy methods for linear systems in higher dimensions.
    #    Part I: SPD systems, http://arxiv.org/abs/1301.6068,
    #    Part II: Faster algorithm and application to nonsymmetric systems, http://arxiv.org/abs/1304.1222
    #
    #    Use {sergey.v.dolgov, dmitry.savostyanov}@gmail.com for feedback
    # ********
    #
    #  TT-Toolbox 2.2, 2009-2012
    #
    # This is TT Toolbox, written by Ivan Oseledets et al.
    # Institute of Numerical Mathematics, Moscow, Russia
    # webpage: http://spring.inm.ras.ru/osel
    #
    # For all questions, bugs and suggestions please mail
    # ivan.oseledets@gmail.com
    # ---------------------------



    d = copy.copy(B.d)
    m = copy.copy(B.n)
    k = copy.copy(B.m)
    ry = copy.copy(B.r)
    Ry = 1
    Y = tt.matrix.to_list(B)
    outtype = 1


    # Grumble matrix
    # Copy TT ranks and blocks from a tt_matrix
    ra = copy.copy(A.r)
    if (A.d!=d) or (not np.array_equal(A.m, m)):
        raise Exception('Mismatching TT dimensionalities in A and Y');
    n = copy.copy(A.n)
    Ra = 1
    A = tt.matrix.to_list(A)

    for i in range(d):
        A[i] = A[i].reshape(ra[i]*n[i], m[i]*ra[i+1], order='f')


    # Initial guess
    if x0 is None:
        X = tt.rand(n*k, d=d, r=copy.copy(ry)) # Check
        rx = X.r
        X = tt.vector.to_list(X)
        init_qr = True
    else:
        # X was given, parse it
        X = copy.copy(x0)
        if (not np.array_equal(X.n, n)) or (not np.array_equal(X.m, k)):
            raise Exception('Mismatching dimensions in x0');
        rx = copy.copy(X.r)
        X = tt.matrix.to_list(X)


    # Reductions
    XAY = [[] for i in range(d+1)]
    ZAY = [[] for i in range(d+1)]
    ZX = [[] for i in range(d+1)]
    XAY[0] = np.ones((Ra, Ry))
    XAY[d] = np.ones((Ra, Ry))
    ZAY[0] = np.ones((Ra, Ry))
    ZAY[d] = np.ones((Ra, Ry))
    ZX[0] = 1
    ZX[d] = 1

    # Residual rank
    rz = [1] + [kickrank]*(d-1) + [1]

    nrms = np.ones(d); # To prevent doulbe overflow

    # Initial ort
    for i in range(d-1):
        if init_qr:
            cr = X[i].reshape(rx[i]*n[i]*k[i], rx[i+1], order='f')
            cr, R = np.linalg.qr(cr)
            nrmr = np.linalg.norm(R, ord='fro')
            if nrmr > 0:
                R = R / nrmr
            cr2 = X[i+1].reshape(rx[i+1], n[i+1]*k[i+1]*rx[i+2], order='f')
            cr2 = R.dot(cr2)
            rx[i+1] = cr.shape[1]
            X[i] = cr.reshape(rx[i], n[i], k[i], rx[i+1], order='f')
            X[i+1] = cr2.reshape(rx[i+1], n[i+1], k[i+1], rx[i+2], order='f')
        # Reduce
        XAY[i+1], nrms[i] = leftreduce_matrix(XAY[i], X[i], A[i], Y[i],
                                              rx[i], n[i], k[i], rx[i+1],
                                              Ra, ra[i], ra[i+1], Ry, ry[i], m[i], ry[i+1])
        # Residual reductions
        if kickrank > 0:
            cr = np.random.randn(rz[i]*n[i]*k[i], rz[i+1])
            cr, _ = np.linalg.qr(cr)
            rz[i+1] = cr.shape[1]
            ZAY[i+1], _ = leftreduce_matrix(ZAY[i], cr, A[i], Y[i],
                                            rz[i], n[i], k[i], rz[i+1],
                                            Ra, ra[i], ra[i+1], Ry, ry[i], m[i], ry[i+1], nrms[i], use_nrm=False)
            ZX[i+1] = leftreduce_vector(ZX[i], cr, X[i], rz[i], n[i], k[i], rz[i+1], 1, rx[i], rx[i+1])



    i = d-1 #check
    di = -1
    swp = 1
    max_dx = 0
    # Iteration
    while swp <= nswp:
        # Project the MatVec generating vector
        cr = local_matvec(Y[i], Ry, ry[i], m[i], k[i],
                          ry[i+1], rx[i], n[i], rx[i+1],
                          XAY[i], A[i], XAY[i+1], Ra, ra[i], ra[i+1])
        nrms[i] = np.linalg.norm(cr, ord='fro')
        # The main goal is to keep y{i} of norm 1
        if nrms[i] > 0:
            cr = cr / nrms[i]
        else:
            nrms[i] = 1
        X[i] = X[i].reshape(rx[i]*n[i]*k[i]*rx[i+1], 1, order='f')
        dx = np.linalg.norm(cr - X[i])
        max_dx = max(max_dx, dx)

        # Truncation and enrichment
        if (di > 0) and (i < d-1): # here
            cr = cr.reshape(rx[i]*n[i]*k[i], rx[i+1], order='f')
            u, s, v = np.linalg.svd(cr)
            v = v.T.conj()
            r = my_chop2(s, tol*np.linalg.norm(s) / np.sqrt(d))
            u = u[:, :r]
            v = v[:, :r].conj().dot(np.diag(s[:r]))

            # Prepare enrichment, if needed
            if kickrank > 0:
                cr = u.dot(v.T)
                cr = cr.reshape(rx[i]*n[i]*k[i], rx[i+1], order='f')
                # For updating z
                crz = local_matvec(Y[i], Ry, ry[i], m[i], k[i], ry[i+1], rz[i], n[i], rz[i+1],
                                   ZAY[i], A[i], ZAY[i+1], Ra, ra[i], ra[i+1])
                crz = crz.reshape(rz[i]*n[i]*k[i], rz[i+1], order='f')
                ys = cr.dot(ZX[i+1])
                yz = ys.reshape(rx[i], n[i]*k[i]*rz[i+1], order='f')
                yz = np.array(ZX[i]).dot(yz)
                yz = yz.reshape(rz[i]*n[i]*k[i], rz[i+1], order='f')
                crz = crz / nrms[i] - yz
                nrmz = np.linalg.norm(crz, ord='fro')
                # For adding into solution
                crs = local_matvec(Y[i], Ry, ry[i], m[i], k[i], ry[i+1], rx[i], n[i], rz[i+1],
                                   XAY[i], A[i], ZAY[i+1], Ra, ra[i], ra[i+1])
                crs = crs.reshape(rx[i]*n[i]*k[i], rz[i+1], order='f')
                crs = crs / nrms[i] - ys
                u = np.concatenate((u, crs), axis=1) #check
                u, R = np.linalg.qr(u)
                v = np.concatenate((v, np.zeros((rx[i+1], rz[i+1]))), axis=1);
                v = v.dot(R.T)
                r = u.shape[1]

            X[i] = u.reshape(rx[i], n[i], k[i], r, order='f')
            cr2 = X[i+1].reshape(rx[i+1], n[i+1]*k[i+1]*rx[i+2], order='f')
            v = v.reshape(rx[i+1], r, order='f')
            cr2 = v.T.dot(cr2)
            X[i+1] = cr2.reshape(r, n[i+1], k[i+1], rx[i+2], order='f')
            rx[i+1] = copy.copy(r)

            nrms[i+di] = nrms[i] # this is the norm of my block, save it
            # Reduce
            XAY[i+1], nrms[i] = leftreduce_matrix(XAY[i], X[i], A[i], Y[i], rx[i], n[i], k[i], rx[i+1],
                                                  Ra, ra[i], ra[i+1], Ry, ry[i], m[i], ry[i+1])
            # Enrichment
            if kickrank > 0:
                crz, _ = np.linalg.qr(crz)
                rz[i+1] = crz.shape[1]
                ZAY[i+1], _ = leftreduce_matrix(ZAY[i], crz, A[i], Y[i],
                                                rz[i], n[i], k[i], rz[i+1],
                                                Ra, ra[i], ra[i+1], Ry, ry[i], m[i], ry[i+1],
                                                nrms[i], use_nrm=False)
                ZX[i+1] = leftreduce_vector(ZX[i], crz, X[i], rz[i], n[i], k[i], rz[i+1], 1, rx[i], rx[i+1]);

        elif (di < 0) and (i > 0):
            cr = cr.reshape(rx[i], n[i]*k[i]*rx[i+1], order='f')
            u, s, v = np.linalg.svd(cr)
            v = v.T.conj()
            r = my_chop2(s, tol*np.linalg.norm(s) / np.sqrt(d))
            v = v[:, :r].conj()
            u = u[:, :r].dot(np.diag(s[:r]))


            # Prepare enrichment, if needed
            if kickrank > 0:
                cr = u.dot(v.T)
                cr = cr.reshape(rx[i], n[i]*k[i]*rx[i+1], order='f')
                # For updating z
                crz = local_matvec(Y[i], Ry, ry[i], m[i], k[i], ry[i+1],
                                   rz[i], n[i], rz[i+1], ZAY[i], A[i], ZAY[i+1], Ra, ra[i], ra[i+1])
                crz = crz.reshape(rz[i], n[i]*k[i]*rz[i+1], order='f')
                ys = ZX[i].dot(cr)
                yz = ys.reshape(rz[i]*n[i]*k[i], rx[i+1], order='f')
                yz = yz.dot(ZX[i+1])
                yz = yz.reshape(rz[i], n[i]*k[i]*rz[i+1], order='f')
                crz = crz / nrms[i] - yz
                nrmz = np.linalg.norm(crz, ord='fro')
                # For adding into solution
                crs = local_matvec(Y[i], Ry, ry[i], m[i], k[i], ry[i+1], rz[i], n[i],
                                   rx[i+1], ZAY[i], A[i], XAY[i+1], Ra, ra[i], ra[i+1])
                crs = crs.reshape(rz[i], n[i]*k[i]*rx[i+1], order='f')
                crs = crs / nrms[i] - ys
                v = np.concatenate((v, crs.T), axis=1)
                v, R = np.linalg.qr(v)
                u = np.concatenate((u, np.zeros((rx[i], rz[i]))), axis=1)
                u = u.dot(R.T)
                r = v.shape[1]
            cr2 = X[i-1].reshape(rx[i-1]*n[i-1]*k[i-1], rx[i], order='f')
            cr2 = cr2.dot(u)
            X[i-1] = cr2.reshape(rx[i-1], n[i-1], k[i-1], r, order='f')
            X[i] = v.T.reshape(r, n[i], k[i], rx[i+1], order='f')

            rx[i] = copy.copy(r)

            nrms[i+di] = nrms[i] # this is the norm of my block, save it
            # Reduce
            XAY[i], nrms[i] = rightreduce_matrix(XAY[i+1], X[i], A[i], Y[i],
                                                 rx[i], n[i], k[i], rx[i+1],
                                                 Ra, ra[i], ra[i+1], Ry, ry[i], m[i], ry[i+1])
            # Enrich
            if kickrank > 0:
                crz, _ = np.linalg.qr(crz.T)
                rz[i] = crz.shape[1]
                ZAY[i], _ = rightreduce_matrix(ZAY[i+1], crz, A[i], Y[i],
                                               rz[i], n[i], k[i], rz[i+1], Ra, ra[i], ra[i+1],
                                               Ry, ry[i], m[i], ry[i+1], nrms[i], use_nrm=False)
                ZX[i] = rightreduce_vector(ZX[i+1], crz, X[i], rz[i], n[i], k[i], rz[i+1], 1, rx[i], rx[i+1])
        else:
            X[i] = cr.reshape(rx[i], n[i], k[i], rx[i+1], order='f')

        if verb > 1:
            print('amen-mm: swp=[{0:},{1:}], dx={2:}, r={3:}, |X|={4:}, |z|={5:}'
                  .format(swp, i, dx, r, np.linalg.norm(cr, ord='fro'), nrmz))

        # Stopping or reversing
        if (di > 0 and i==d-1) or (di < 0 and i==0):
            if verb > 0:
                print('amen-mm: swp={0:}/{1:}, max_dx={2:}, max_r={3:}'
                      .format(swp, (1.-di)/2, max_dx, np.max(rx)))

            if ((max_dx < tol) or (swp==nswp)) and (di > 0):
                break
            swp = swp + 1
            max_dx = 0
            di = -di
        else:
            i = i + di

    # Distribute norms equally...
    nrms = np.exp(np.sum(np.log(nrms))/d)
    # ... and plug them into y
    for i in range(d):
        X[i] = X[i] * nrms # check: * is * or .dot

    # Return the correct form
    X = tt.matrix.from_list(X)

    return X


# Accumulates the left reduction W{1:k}'*A{1:k}*X{1:k}
def leftreduce_matrix(WAX, w2, A, x, rw1, n, k, rw2, Ra, ra1, ra2, Rx, rx1, m, rx2, extnrm=None, use_nrm=True):
    # Left WAX has the form of the first matrix TT block, i.e. [rw, rx, ra]

    nrm = 0

    w = w2.reshape(rw1*n, k, rw2, order='f')
    w = np.transpose(w, [0, 2, 1])
    w = w.reshape(rw1, n*rw2*k, order='f')
    xc = x.reshape(rx1*m, k, rx2, order='f')
    xc = np.transpose(xc, [2, 1, 0])
    xc = xc.reshape(rx2, k*rx1*m, order='f')
    WAX2 = WAX.reshape(rw1, rx1*ra1, order='f')
    WAX2 = w.T.dot(WAX2) # size n rw2 x rx1 ra1
    WAX2 = WAX2.reshape(n, rw2*k*rx1*ra1, order='f')
    WAX2 = WAX2.T
    WAX2 = WAX2.reshape(rw2*k*rx1, ra1*n, order='f')
    WAX2 = WAX2.dot(A) # size rw2 rx1 m ra2
    WAX2 = WAX2.reshape(rw2, k*rx1*m*ra2, order='f')
    WAX2 = WAX2.T
    WAX2 = WAX2.reshape(k*rx1*m, ra2*rw2, order='f')
    WAX2 = xc.dot(WAX2) # size rx2, ra2 rw2
    WAX2 = WAX2.reshape(rx2*ra2, rw2, order='f')
    WAX2 = WAX2.T
    # WAX2{i,j} = reshape(WAX2{i,j}, rw2, rx2(j), ra2(i));
    if use_nrm:
        nrm = max(nrm, np.linalg.norm(WAX2, ord='fro'))

    if use_nrm:
        # Extract the scale to prevent overload
        if nrm > 0:
            WAX2 = WAX2/nrm;
        else:
            nrm = 1

    elif extnrm: #check
        # Override the normalization
        WAX2 = WAX2 / extnrm

    return WAX2, nrm



# Accumulates the right reduction W{k:d}'*A{k:d}*X{k:d}
def rightreduce_matrix(WAX, w1, A, x, rw1, n, k, rw2, Ra, ra1, ra2, Rx, rx1, m, rx2, extnrm=None, use_nrm=True):
    # Right WAX has the form of the last matrix TT block, i.e. [ra, rw, rx]

    nrm = 0

    w = w1.reshape(rw1*n, k, rw2, order='f')
    w = np.transpose(w, [0, 2, 1])
    w = w.reshape(rw1, n*rw2*k, order='f')
    w = w.conj()

    xc = x.reshape(rx1*m, k, rx2, order='f')
    xc = np.transpose(xc, [1, 0, 2])
    xc = xc.reshape(k*rx1*m, rx2, order='f')

    WAX1 = WAX.reshape(ra2*rw2, rx2, order='f')
    WAX1 = xc.dot(WAX1.T) # size rx1 m x ra2 rw2
    WAX1 = WAX1.reshape(k*rx1, m*ra2*rw2, order='f')
    WAX1 = WAX1.T
    WAX1 = WAX1.reshape(m*ra2, rw2*k*rx1, order='f')
    WAX1 = A.dot(WAX1) # size ra1(k)*n, rw2*rx1
    WAX1 = WAX1.reshape(ra1, n*rw2*k*rx1, order='f')
    WAX1 = WAX1.T
    WAX1 = WAX1.reshape(n*rw2*k, rx1*ra1, order='f')
    WAX1 = w.dot(WAX1) # size rw1, rx1 ra1
    WAX1 = WAX1.reshape(rw1*rx1, ra1, order='f')
    WAX1 = WAX1.T
    # WAX1{i,j} = reshape(WAX1{i,j}, ra1(i), rw1, rx1(j));
    if use_nrm:
        nrm = max(nrm, np.linalg.norm(WAX1, ord='fro'))

    if use_nrm:
        # Extract the scale to prevent overload
        if nrm > 0:
            WAX1 = WAX1 / nrm
        else:
            nrm = 1
    elif extnrm: # check
        # Override the normalization
        WAX1 = WAX1 / extnrm

    return WAX1, nrm


# Accumulates the left reduction W{1:k}'*X{1:k}
def leftreduce_vector(WX1, w, x, rw1, n, k, rw2, Rx, rx1, rx2, extnrm=None):
    # Left WX has the form of the first vector TT block, i.e. [rw, rx]

    WX2 = copy.copy(WX1)
    wc = w.reshape(rw1, n*k*rw2, order='f')

    WX2 = wc.T.dot(WX2) # size n rw2 x rx1
    WX2 = WX2.reshape(n*k, rw2*rx1, order='f')
    WX2 = WX2.T
    WX2 = WX2.reshape(rw2, rx1*n*k, order='f')
    tmp = x.reshape(rx1*n*k, rx2, order='f')
    WX2 = WX2.dot(tmp) # size rw2, rx2

    if extnrm:
        # Override the normalization
        WX2 = WX2 / extnrm

    return WX2


# Accumulates the right reduction W{k:d}'*X{k:d}
def rightreduce_vector(WX2, w, x, rw1, n, k, rw2, Rx, rx1, rx2, extnrm=None):
    # Right WX has the form of the last vector TT block, i.e. [rx, rw]

    WX1 = copy.copy(WX2)
    wc = w.reshape(rw1, n*k*rw2, order='f')
    tmp = x.reshape(rx1*n*k, rx2, order='f')
    WX1 = tmp.dot(WX1) # size rx1 n x rw2
    WX1 = WX1.reshape(rx1, n*k*rw2, order='f')
    WX1 = WX1.dot(wc.T.conj()) # size rx1, rw1 # CHECK! Used to be wc' instead of wc.'

    if extnrm:
        # Override the normalization
            WX1 = WX1 / extnrm

    return WX1


# A matrix-matrix product for the matrix in the 3D TT (WAX1-A-WAX2), and
# full matrix of size (rx1*m*k*rx2). Returns (rw1*n*k*rw2)
def local_matvec(x, Rx, rx1, m, k, rx2, rw1, n, rw2, WAX1, A, WAX2, Ra, ra1, ra2):

    w = np.zeros((rw1*n*rw2, k))
    xc = x.reshape(rx1*m, k, rx2, order='f')
    xc = np.transpose(xc, [1, 0, 2]);
    xc = xc.reshape(k*rx1*m, rx2, order='f')

    tmp = WAX2.reshape(ra2*rw2, rx2, order='f')
    wk = xc.dot(tmp.T)
    wk = wk.reshape(k*rx1, m*ra2*rw2, order='f')
    wk = wk.T
    wk = wk.reshape(m*ra2, rw2*k*rx1, order='f')
    wk = A.dot(wk)
    wk = wk.reshape(ra1*n*rw2*k, rx1, order='f')
    wk = wk.T
    wk = wk.reshape(rx1*ra1, n*rw2*k, order='f')
    tmp = WAX1.reshape(rw1, rx1*ra1, order='f')
    wk = tmp.dot(wk)
    wk = wk.reshape(rw1*n*rw2, k, order='f')
    w = w + wk

    w = w.reshape(rw1*n, rw2, k, order='f')
    w = np.transpose(w, [0, 2, 1])
    w = w.reshape(-1, 1, order='f')
    return w


def my_chop2(sv, eps):
# Truncation by absolution precision in Frobenius norm
#    [R]=MY_CHOP2(SV,EPS) truncation by absolute precision in the Frobenius
#    norm. Finds minimal possible R such that \sqrt(sum(SV(R:n)^2)) < EPS
#
#
#  TT-Toolbox 2.2, 2009-2012
#
# This is TT Toolbox, written by Ivan Oseledets et al.
# Institute of Numerical Mathematics, Moscow, Russia
# webpage: http://spring.inm.ras.ru/osel
#
# For all questions, bugs and suggestions please mail
# ivan.oseledets@gmail.com
# ---------------------------
# Vectorize this supercode
#  Check for zero tensor

    if np.linalg.norm(sv) == 0:
        r = 1
        return r

    # Check for zero tolerance
    if eps <= 0:
        r = s.size
        return r

    sv0 = np.cumsum(sv[::-1]**2)
    ff = np.where(sv0 < eps**2)[0]
    if ff.size == 0:
        r = sv.size
    else:
        r = sv.size - (ff[-1] + 1) #check
    return r
