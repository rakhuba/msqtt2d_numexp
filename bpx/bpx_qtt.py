import numpy as np
import time
import copy
import tt
import bpx


def inv(perm):
    inverse = [0] * len(perm)
    for i, p in enumerate(perm):
        inverse[p] = i
    return inverse


def get_lam(coef, m=1./3):
    L = coef.d
    lam = tt.diag(coef)
    crs_lam = tt.matrix.to_list(lam)
    for i in range(L):
        crs_lam[i] *= 2**(-D)
    crs_lam = [np.eye(2).reshape(1, 2, 2, 1, order='f')] + crs_lam + [np.eye(1).reshape(1, 1, 1, 1, order='f')]
    crs_lam[0][0, -1, -1, 0] *= m
    lam = tt.matrix.from_list(crs_lam)
    return lam


def prolongate(x, D, order=None):

    if x.n[1] > 2:
        order = 'transposed'
        L = x.d
    elif x.n[1] == 2:
        order = 'normal'
        L = x.d / D
    else:
        raise NotImplementedError




    I = np.eye(2)
    O = np.zeros((2, 2))
    J = np.zeros((2, 2))
    J[0, 1] = 1.

    cr0 = np.zeros((1, 2, 1, 2))
    cri = np.zeros((2, 2, 2, 2))
    crL = np.zeros((2, 2, 2, 1))

    crL[0, :, :, 0] = I
    crL[1, :, :, 0] = J.T

    cri[0, :, :, 0] = I
    cri[1, :, :, 0] = J.T
    cri[0, :, :, 1] = O
    cri[1, :, :, 1] = J

    cr0[0, :, :, 0] = np.array([[1], [2]])
    cr0[0, :, :, 1] = np.array([[1], [0]])

    crs = [0.5*cr0] + [cri]*(L-1) + [crL]

    P1 = tt.matrix.from_list(crs)

    if order == 'transposed':
        P = P1
        if D > 1:
            for i in range(D-1):
                P = bpx.zkronm(P, P1)

    elif order == 'time tr 2D':
        P = tt.kron(P1, bpx.zkronm(P1, P1))

    elif order == 'normal':
        P = P1
        if D > 1:
            for i in range(D-1):
                P = tt.kron(P, P1)
    crs = tt.vector.to_list(x)
    if order == 'normal':
        if D == 1:
            crs = [np.ones((1, 1, 1))] + crs
        elif D == 2:
            rL = crs[L-1].shape[-1]
            crs = [np.ones((1, 1, 1))] + crs[:L] + [np.eye(rL).reshape(rL, 1, rL, order='f')] + crs[L:]
        elif D == 3:
            rL1 = crs[L-1].shape[-1]
            rL2 = crs[2*L-1].shape[-1]
            crs = ([np.ones((1, 1, 1))] + crs[:L] +
                   [np.eye(rL1).reshape(rL1, 1, rL1, order='f')] + crs[L:2*L] +
                   [np.eye(rL2).reshape(rL2, 1, rL2, order='f')] + crs[2*L:])
        else:
            raise NotImplementedError
        t = tt.vector.from_list(crs)#.round(1e-14)
        y = tt.matvec(P, t)

    elif order == 'transposed':
        crs = [np.ones((1, 1, 1))] + crs
        t = tt.vector.from_list(crs)
        y = tt.matvec(P, t)

    elif order == 'time tr 2D':
        crs = [np.ones((1, 1, 1))] + crs
        t = tt.vector.from_list(crs)
        y = tt.matvec(P, t)

    return y


def prolongation(L, D, order='transposed'):
    I = np.eye(2)
    O = np.zeros((2, 2))
    J = np.zeros((2, 2))
    J[0, 1] = 1.

    cr0 = np.zeros((1, 2, 1, 2))
    cri = np.zeros((2, 2, 2, 2))
    crL = np.zeros((2, 2, 2, 1))

    crL[0, :, :, 0] = I
    crL[1, :, :, 0] = J.T

    cri[0, :, :, 0] = I
    cri[1, :, :, 0] = J.T
    cri[0, :, :, 1] = O
    cri[1, :, :, 1] = J

    cr0[0, :, :, 0] = np.array([[1], [2]])
    cr0[0, :, :, 1] = np.array([[1], [0]])

    crs = [0.5*cr0] + [cri]*(L-1) + [crL, np.ones((1, 1, 1, 1))]

    P1 = tt.matrix.from_list(crs)

    if order == 'transposed':
        P = P1
        if D > 1:
            for i in range(D-1):
                P = zkronm(P, P1)

    elif order == 'normal':
        P = P1
        if D > 1:
            for i in range(D-1):
                P = tt.kron(P, P1)

    return P


def to_full(a):
    L = a.d - 2
    n = a.n[1]
    if n == 2:
        a_full = a.full().flatten(order='f')
    elif n == 4:
        a_full = a.full().reshape([2]*2*L, order='f')
        a_full = np.transpose(a_full, range(0, 2*L, 2) + range(1, 2*L+1, 2))
        a_full = a_full.reshape([2**L, 2**L], order='f')
    elif n == 8:
        a_full = a.full().reshape([2]*3*L, order='f')
        a_full = np.transpose(a_full, range(0, 3*L, 3) + range(1, 3*L+1, 3) + range(2, 3*L+2, 3) )
        a_full = a_full.reshape([2**L, 2**L, 2**L], order='f')
    return a_full


def pad(f, side='lr'):
    crs = tt.vector.to_list(f)
    if side == 'lr':
        crs = [np.ones((1, 1, 1))] + crs + [np.ones((1, 1, 1))]
    elif side == 'l':
        crs = [np.ones((1, 1, 1))] + crs
    elif side == 'r':
        crs = crs + [np.ones((1, 1, 1))]
    else:
        raise NotImplementedError
    return tt.vector.from_list(crs)


def mode_core_product(A, B):
    res = [
        [A[0][0].dot(B[0][0]), A[0][0].dot(B[0][1]), A[0][1].dot(B[0][0]), A[0][1].dot(B[0][1])],
        [A[0][0].dot(B[1][0]), A[0][0].dot(B[1][1]), A[0][1].dot(B[1][0]), A[0][1].dot(B[1][1])],
        [A[1][0].dot(B[0][0]), A[1][0].dot(B[0][1]), A[1][1].dot(B[0][0]), A[1][1].dot(B[0][1])],
        [A[1][0].dot(B[1][0]), A[1][0].dot(B[1][1]), A[1][1].dot(B[1][0]), A[1][1].dot(B[1][1])],
    ]
    res = np.bmat(res)
    res = np.array(res)
    return res


def _mkron(l):
    D = len(l)
    res = l[0]
    if D == 1:
        pass
    else:
        for i in range(1, D):
            res = np.kron(res, l[i])
    return res


def _bkron(A, D, num_blocks, block_size):

    if D == 1:
        return A

    else:
        A4 = A.reshape([block_size[0], num_blocks[0],
                        block_size[1], num_blocks[1]], order='f')

        res = copy.copy(A4)
        for i in range(1, D):
            res = np.kron(res, A4)
        res_size = [(block_size[0] * num_blocks[0]) ** D,
                    (block_size[1] * num_blocks[1]) ** D]
        res = res.reshape(res_size[0], res_size[1], order='f')
    return res


def tracy_singh(a, b, bsize_a, bsize_b):

    nblocks_a = [a.shape[0] / bsize_a[0], a.shape[1] / bsize_a[1]]
    nblocks_b = [b.shape[0] / bsize_b[0], b.shape[1] / bsize_b[1]]

    block_a = np.split(a, nblocks_a[0], axis=0)
    len_block_a0 = len(block_a)
    for i in range(len_block_a0):
        block_a[i] = np.split(block_a[i], nblocks_a[1], axis=1)
    len_block_a1 = len(block_a[0])

    block_b = np.split(b, nblocks_b[0], axis=0)
    len_block_b0 = len(block_b)
    for i in range(len_block_b0):
        block_b[i] = np.split(block_b[i], nblocks_b[1], axis=1)
    len_block_b1 = len(block_b[0])

    res = [0] * (len_block_a0*len_block_b0)
    for i in range(len(res)):
        res[i] = [0] * (len_block_a1*len_block_b1)
    for i0 in range(len_block_a0):
        for j0 in range(len_block_a1):
            for i1 in range(len_block_b0):
                for j1 in range(len_block_b1):
                    res[i1+len_block_b0*i0][j1+len_block_b1*j0] = np.kron(block_a[i0][j0], block_b[i1][j1])

    return np.array(np.bmat(res))


def aux_matrices(D, alpha=None, bc='dn', kron='1->D'):

    A = np.array([[1, 0]], dtype=np.float64)
    if bc == 'dn':
        Ab = np.array([[1, 0, 0, 0]], dtype=np.float64)
    elif bc == 'dd':
        Ab = np.array([[1, 0, 0, 0, 0]], dtype=np.float64)
    Pb = np.array([[1, 0, 0, 0]], dtype=np.float64).T

    T0 = np.array([[1, 1], [1, -1]], dtype=np.float64)
    T1 = np.array([[1], [-1]], dtype=np.float64)

    I = np.array([[1, 0], [0, 1]], dtype=np.float64)
    O = np.array([[0, 0], [0, 0]], dtype=np.float64)
    J = np.array([[0, 1], [0, 0]], dtype=np.float64)
    I1 = np.array([[1, 0], [0, 0]], dtype=np.float64)
    I2 = np.array([[0, 0], [0, 1]], dtype=np.float64)


    if bc == 'dn':
        U_list = [[I, J.T], [O, J]]
        UT_list = [[I, J], [O, J.T]]
        Vb = None
        U = np.array(np.bmat(U_list))
        Ub = mode_core_product(U_list, UT_list)
    elif bc == 'dd':
        Ub_list = [[I, J, O, J.T, I2],
                  [O, J.T, O, O, O],
                  [I1, J, I2, J.T, I2],
                  [O, O, O, J, O],
                  [O, O, O, O, I1]]
        Vb_list = [[I, J, J.T, I2],
                  [O, J.T, O, O],
                  [I1, J, J.T, I2],
                  [O, O, J, O],
                  [O, O, O, I1]]
        Vb = np.array(np.bmat(Vb_list))
        Ub = np.array(np.bmat(Ub_list))

    #UT = [[I, O], [J, J.T]]
    x00 = np.array([[1], [2]], dtype=np.float64)
    x01 = np.array([[0], [1]], dtype=np.float64)
    x10 = np.array([[1], [0]], dtype=np.float64)
    x11 = np.array([[2], [1]], dtype=np.float64)
    X_list = [[x00, x01], [x10, x11]]
    #XT = [[x00.T, x10.T], [x01.T, x11.T]]
    XT_list = [[x00.T, x01.T], [x10.T, x11.T]]

    Xb = 1./4 * mode_core_product(X_list, XT_list)

    M = [
        0.5*np.array([[1, 0, 0, 1]], dtype=np.float64).T,
        np.array([[0, 1]], dtype=np.float64).T
    ]

    W0 = np.array([
        [1, 0, 1, 0],
        [0, 1, 0, 1],
        [1, 0, -1, 0],
        [0, 1, 0, -1]
    ], dtype=np.float64)

    W1 = np.array([
        [1, 0],
        [0, 1],
        [-1, 0],
        [0, -1]
    ], dtype=np.float64)

    Y0_list = [
        [np.ones((2, 1)), np.zeros((2, 1))],
        [0.5*np.array([[-1], [1]], dtype=np.float64), 0.5*np.ones((2, 1))]
     ]

    Z0 = 0.5*mode_core_product(Y0_list, XT_list)
    Z1 = 0.25 * np.array([
            [1, 2, 0, 1],
            [1, 2, 0, 1],
            [1, 0, 2, 1],
            [1, 0, 2, 1]
        ], dtype=np.float64)

    K1 = np.array([[1, 0]], dtype=np.float64).T
    K0 = 0.5*np.array([[1, 0, 0, 0, 0, 1, 0, 0]], dtype=np.float64).T

    W = [W0, W1]
    Z = [Z0, Z1]
    K = [K0, K1]

    AbW0 = np.array([[1, 0, 1, 0]], dtype=np.float64)
    AbW1 = np.array([[1, 0]], dtype=np.float64)
    AbW = [AbW0, AbW1]

    if bc == 'dn':
        UbW0 = np.array(np.bmat([
            [I+J.T, J+J.T.dot(J), I-J.T, J-J.T.dot(J)],
            [O, J.T, O, J.T],
            [J, O, -J, O],
            [O, J.dot(J.T), O, -J.dot(J.T)]
        ]))
        UbW1 = np.array(np.bmat([
            [I-J.T, J-J.T.dot(J)],
            [O, J.T],
            [-J, O],
            [O, -J.dot(J.T)]
        ]))
    elif bc == 'dd':
        UbW0 = np.array(np.bmat([
            [I+J.T, J+I2, I-J.T, J-I2],
            [O, J.T, O, J.T],
            [I1+J.T, I2+J, I1-J.T, J-I2],
            [J, O, -J, O],
            [O, I1, O, -I1]
        ]))
        UbW1 = np.array(np.bmat([
            [I-J.T, J-I2],
            [O, J.T],
            [I1-J.T, J-I2],
            [-J, O],
            [O, -J.dot(J.T)]
        ]))
    UbW = [UbW0, UbW1]

    AbD = Ab
    UbD = Ub
    XbD = Xb
    PbD = Pb
    if bc == 'dd':
        VbD = Vb
    else:
        VbD = None
    for i in range(1, D):
        AbD = tracy_singh(AbD, Ab, [1, 1], [1, 1])
        UbD = tracy_singh(UbD, Ub, [2**i, 2**i], [2, 2])
        XbD = tracy_singh(XbD, Xb, [2**i, 2**i], [2, 2])
        PbD = tracy_singh(PbD, Pb, [1, 1], [1, 1])
        if bc == 'dd':
            VbD = tracy_singh(VbD, Vb, [2**i, 2**i], [2, 2])

    if alpha is not None:
        sum_alpha = np.sum(alpha)

        # WbD
        WbD = W[alpha[0]]
        for i in range(1, D):
            if kron == '1->D':
                #WbD = np.kron(WbD, W[alpha[i]])
                WbD = tracy_singh(WbD, W[alpha[i]], [1, 1], [1, 1])
            elif kron == 'D->1':
                WbD = tracy_singh(W[alpha[i]], WbD, [1, 1], [1, 1])
            else:
                raise Exception('Wrong key for kron')

        # AbW
        AbWD = AbW[alpha[0]]
        for i in range(1, D):
            if kron == '1->D':
                AbWD = tracy_singh(AbWD, AbW[alpha[i]], [1, 1], [1, 1])
            elif kron == 'D->1':
                AbWD = np.kron(AbW[alpha[i]], AbWD)
            else:
                raise Exception('Wrong key for kron')

        ZbD = Z[alpha[0]]
        for i in range(1, D):
            if kron == '1->D':
                #WbD = np.kron(WbD, W[alpha[i]])
                ZbD = tracy_singh(ZbD, Z[alpha[i]], [2**i, 2**i], [2, 2])
            elif kron == 'D->1':
                ZbD = tracy_singh(Z[alpha[i]], ZbD, [2, 2**i], [2, 2**i])
            else:
                raise Exception('Wrong key for kron')


        KbD = K[alpha[0]]
        for i in range(1, D):
            if kron == '1->D':
                #WbD = np.kron(WbD, W[alpha[i]])
                KbD = tracy_singh(KbD, K[alpha[i]], [2**(i-np.sum(alpha[:i])), 1], [2**(1-alpha[i]), 1])
            elif kron == 'D->1':
                # TO BE FIXED
                KbD = tracy_singh(K[alpha[i]], KbD, [2**(1-alpha[i]), 1], [(2**(1-alpha[0]))**i, 1])
            else:
                raise Exception('Wrong key for kron')

        UbWD = UbW[alpha[0]]
        for i in range(1, D):
            if kron == '1->D':
                #WbD = np.kron(WbD, W[alpha[i]])
                UbWD = tracy_singh(UbWD, UbW[alpha[i]], [2**i, 2**i], [2, 2])
            elif kron == 'D->1':
                # TO BE FIXED
                raise NotImplementedError
            else:
                raise Exception('Wrong key for kron')

    else:
        WbD = None
        AbWD = None
        ZbD = None
        KbD = None
        UbWD = None

    res = {
        'Ab': AbD, 'Ub': UbD, 'Xb': XbD, 'Pb': PbD, 'Vb': VbD,
        'Wb': WbD, 'Zb': ZbD, 'Kb': KbD, 'UbW': UbWD, 'AbW': AbWD,
        'Z': Z, 'K': K, 'UW': UbW, 'AW': AbW, 'A': Ab, 'W': W
    }

    return res


def get_Q_tt(L, alpha, bc='dn', eps=False, p=None, const=1.0):

    D = len(alpha)
    mats_dict = aux_matrices(D, bc=bc, alpha=alpha)

    Ab = mats_dict['Ab']
    AbW = mats_dict['AbW']
    Ub = mats_dict['Ub']
    UbW = mats_dict['UbW']
    Zb = mats_dict['Zb']
    Kb = mats_dict['Kb']

    if p is None:
        if D == 1:
            p = [2, 0, 1, 3]
        elif D == 2:
            p = [4, 3, 0, 2, 1, 5]
        elif D == 3:
            p = [6, 5, 4, 0, 3, 2, 1, 7]
        else:
            raise NotImplementedError
        p = list(p)


    sum_alpha = np.sum(alpha)
    if bc == 'dn':
        r = 4 ** (D) + 2 ** (2*D - sum_alpha)
    else:
        r = 5 ** (D) + 2 ** (2*D - sum_alpha)

    modes_0 = np.array([r] + [1]*D + [1]*D + [1])
    modes_inter = np.array([r] + [2]*D + [2]*D + [r])
    modes_1 = np.array([1] + [2**(1-alpha[i]) for i in range(D)] + [1]*D + [r])

    crs_inter = []

    if eps:
        inds_eps = np.where(2.0 ** (-np.arange(1, L+1)) <= eps)[0]
        if inds_eps.size > 0:
            Leps = inds_eps[0]
        else:
            Leps = L

    for i in range(L):
        if eps:
            if i < Leps:
                mult = eps**const * 2.**(sum_alpha*(i+1) + 0.5*D)
            else:
                mult = 2.**(-(i+1)*const+sum_alpha*(i+1) + 0.5*D)
        else:
            mult = 2.**(-(i+1)*const+sum_alpha*(i+1) + 0.5*D)

        cr = np.array(np.bmat([
            [2.**(0.5*D) * Ub, mult * UbW],
            [np.zeros((Zb.shape[0], Ub.shape[1])), 2.**(sum_alpha - 0.5*D) * Zb]
        ]))
        crs_inter.append(cr.T.reshape(modes_inter[p], order='f').transpose(inv(p)).reshape([r, 2**D, 2**D, r], order='f'))

    if eps:
        mult = eps**const
    else:
        mult = 1.

    nD = 2**(D - sum_alpha)
    cr_1 = 1.0 * np.zeros((r*nD, 1))
    cr_1[r*nD-len(Kb):] = Kb
    if bc == 'dd':
        cr_0 = np.zeros(r)
        cr_0[(5**D+1)/2-1] = 1.
    else:
        cr_0 = np.array(np.bmat([[Ab, mult*AbW]]))
    cr_0 = cr_0.T.reshape(modes_0[p], order='f').transpose(inv(p)).reshape([r, 1, 1, 1], order='f')
    cr_1 = cr_1.T.reshape(modes_1[p], order='f').transpose(inv(p)).reshape([1, nD, 1, r], order='f')

    cores = [cr_1] + crs_inter[::-1] + [cr_0]
    Q_tt = tt.matrix.from_list(cores)

    return Q_tt


def get_Ql_tt(L, l, alpha, p=None, kron='1->D'):

    D = len(alpha)
    mats_dict = aux_matrices(D, alpha=alpha, kron=kron)

    Ab = mats_dict['Ab']
    Wb = mats_dict['Wb']
    AbW = mats_dict['AbW']
    Ub = mats_dict['Ub']
    UbW = mats_dict['UbW']
    Zb = mats_dict['Zb']
    Kb = mats_dict['Kb']

    if p is None:
        if D == 1:
            p = [2, 0, 1, 3]
        elif D == 2:
            p = [3, 4, 0, 1, 2, 5]
        elif D == 3:
            p = [6, 5, 4, 0, 3, 2, 1, 7]
        else:
            raise NotImplementedError
        p = list(p)


    sum_alpha = np.sum(alpha)
    r1 = 2 ** (2*D)
    r2 = 2 ** (2*D - sum_alpha)

    modes_0 = np.array([r1] + [1]*D + [1]*D + [1])
    modes_inter1 = np.array([r1] + [2]*D + [2]*D + [r1])
    modes_inter12 = np.array([r1] + [1]*D + [1]*D + [r2])
    modes_inter2 = np.array([r2] + [2]*D + [2]*D + [r2])
    modes_1 = np.array([1] + [2**(1-alpha[i]) for i in range(D)] + [1]*D + [r2])

    nD = 2**(D - sum_alpha)

    core_A = Ab.T.reshape(modes_0[p], order='f').transpose(inv(p)).reshape([r1, 1, 1, 1], order='f')
    core_U = Ub.T.reshape(modes_inter1[p], order='f').transpose(inv(p)).reshape([r1, 2**D, 2**D, r1], order='f')
    core_W = Wb.T.reshape(modes_inter12[p], order='f').transpose(inv(p)).reshape([r2, 1, 1, r1], order='f')
    core_Z = Zb.T.reshape(modes_inter2[p], order='f').transpose(inv(p)).reshape([r2, 2**D, 2**D, r2], order='f')
    core_K = Kb.T.reshape(modes_1[p], order='f').transpose(inv(p)).reshape([1, nD, 1, r2], order='f')

    core_A *= 2. ** (-(1-sum_alpha)*l)
    core_U *= 2. ** (0.5*D)
    core_Z *= 2. ** (sum_alpha-0.5*D)

    cores = [core_K] + [core_Z]*(L-l) + [core_W] + [core_U]*l + [core_A]
    Q_tt = tt.matrix.from_list(cores)

    return Q_tt


def get_C_tt(L, D, const=1.0, bc='dn', eps=False, test=False):

    if bc == 'dn':
        mats_dict = aux_matrices(D, bc='dn')
        Vb = mats_dict['Ub']
    elif bc == 'dd':
        mats_dict = aux_matrices(D, bc='dd')
        Vb = mats_dict['Vb']
    else:
        raise NotImplementedError
    Ab = mats_dict['Ab']
    Ub = mats_dict['Ub']
    Xb = mats_dict['Xb']
    Pb = mats_dict['Pb']

    if D == 1:
        p = [2, 0, 1, 3]
    elif D == 2:
        p = [4, 3, 0, 2, 1, 5]
    elif D == 3:
        p = [6, 5, 4, 0, 3, 2, 1, 7]
    else:
        raise NotImplementedError
    p = list(p)

    #r = 2 ** (2*D + 1)
    if bc == 'dn':
        r = 2 * 4 ** D
    elif bc == 'dd':
        r = 4 ** D + 5 ** D

    modes_0 = np.array([r] + [1]*D + [1]*D + [1])
    modes_inter = np.array([r] + [2]*D + [2]*D + [r])
    modes_1 = np.array([1] + [1]*D + [1]*D + [r])

    crs_inter = []
    if not eps:
        for i in range(L):
            cr = np.array(np.bmat([
                [Ub, 2.**(-(i+1)*const) * Vb],
                [np.zeros((Xb.shape[0], Ub.shape[1])), 2.**(-D) * Xb]
            ]))
            crs_inter.append(cr.T.reshape(modes_inter[p], order='f').transpose(inv(p)).reshape([r, 2**D, 2**D, r], order='f'))
    else:

        inds_eps = np.where(2.0 ** (-np.arange(1, L+1)) <= eps)[0]
        if inds_eps.size > 0:
            Leps = inds_eps[0]
        else:
            Leps = L

        for i in range(L):
            if i < Leps:
                mult = eps**const
            else:
                mult = 2.**(-(i+1)*const)
            cr = np.array(np.bmat([
                [Ub, mult * Vb],
                [np.zeros((Xb.shape[0], Ub.shape[1])), 2.**(-D)*Xb]
            ]))
            crs_inter.append(cr.T.reshape(modes_inter[p], order='f').transpose(inv(p)).reshape([r, 2**D, 2**D, r], order='f'))

    cr_1 = 1.0 * np.zeros((r, 1))
    cr_1[r-len(Pb):] = Pb

    if eps:
        mult = eps**const
    else:
        mult = 1.
    if bc == 'dd':
        cr_0 = np.zeros(r)
        cr_0[(5**D+1)/2-1] = 1.
    else:
        cr_0 = np.array(np.bmat([[Ab, mult*Ab]]))
    cr_0 = cr_0.T.reshape(modes_0[p], order='f').transpose(inv(p)).reshape([r, 1, 1, 1], order='f')
    cr_1 = cr_1.T.reshape(modes_1[p], order='f').transpose(inv(p)).reshape([1, 1, 1, r], order='f')

    cores = [cr_1] + crs_inter[::-1] + [cr_0]
    C_tt = tt.matrix.from_list(cores)

    if test:
        C_full = bpx.bpx_full(L, D)
        C_tt_full = C_tt.full()
        C_tt_full = C_tt_full.reshape([2]*(2*D)*L, order='f')
        inds_tr = []
        for i in range(D):
            inds_tr += range(i, (2*D)*L+i, D)
        C_tt_full = C_tt_full.transpose(inds_tr)
        C_tt_full = C_tt_full.reshape(2 ** (D*L), 2 ** (D*L), order='f')
        if np.linalg.norm(C_tt_full - C_full) / np.linalg.norm(C_full) > 1e-12:
            raise Exception('Test failed')
        else:
            print('Test was successful')
    return C_tt


def zkron(ttA, ttB, eps=1e-12):
    Al = tt.vector.to_list(ttA)
    Bl = tt.vector.to_list(ttB)
    Hl = []
    for (A, B) in zip(Al, Bl):
        H = np.kron(B, A)
        Hl.append(H)
    ttH = tt.vector.from_list(Hl)
    res = ttH.round(eps)
    return res


def zkronm(ttA, ttB, eps=1e-12):
    Al = tt.matrix.to_list(ttA)
    Bl = tt.matrix.to_list(ttB)
    Hl = []
    for (A, B) in zip(Al, Bl):
        H = np.kron(B, A)
        Hl.append(H)
    res = tt.matrix.from_list(Hl)
    # szn = np.concatenate((ttA.n.reshape(1, -1), ttB.n.reshape(1, -1))).flatten(order='f')
    # szm = np.concatenate((ttA.m.reshape(1, -1), ttB.m.reshape(1, -1))).flatten(order='f')
    # res = tt.reshape(res, np.array([szn, szm]).T, eps)
    # res.tt.ps = np.array(res.tt.ps, dtype=np.int32)
    # res = res.round(eps)
    return res


def xi(k): return np.ones(2**k).reshape(-1, 1, order='f')
def eta(k): return (2.0 ** (-k) * np.arange(1, 2**k + 1)).reshape(-1, 1, order='f')
def I(l): return np.eye(2**l)
def S(l): return np.diag(np.ones(2**l-1), -1)


def permute(x_, order, eps):
    x = tt.vector.to_list(x_)
    d = copy.copy(x_.d)
    r = copy.copy(x_.r)
    n = copy.copy(x_.n)
    idx = np.ones(d, dtype=np.int64)
    idx[order] = np.arange(d)
    eps = eps / d ** (1.5)
    if (len(order) < d):
        raise Exception('ORDER must have at least D elements for a D-dimensional tensor')

    for kk in range(d-1, 1, -1):
        Q, R = np.linalg.qr(x[kk].reshape([r[kk], n[kk]*r[kk+1]], order='f').T)
        tr = min([r[kk], n[kk]*r[kk+1]]);
        x[kk] = (Q.T).reshape([tr, n[kk], r[kk+1]], order='f')
        x[kk-1] = (x[kk-1].reshape([r[kk-1]*n[kk-1], r[kk]], order='f').dot(R.T)
                         ).reshape([r[kk-1], n[kk-1], tr], order='f')
        r[kk] = copy.copy(tr)

    k = 0
    while True:
        # Find next inversion
        nk = copy.copy(k)
        while (nk < d - 1) and (idx[nk] < idx[nk+1]):
            nk += 1
        if nk == d - 1:
            break

        # Move orthogonal centre there
        for kk in range(k, nk): # maybe to nk - 1
            Q, R = np.linalg.qr(x[kk].reshape([r[kk]*n[kk], r[kk+1]], order='f'))
            tr = min([r[kk]*n[kk], r[kk+1]])
            x[kk] = Q.reshape([r[kk], n[kk], tr], order='f')
            x[kk + 1] = (R.dot(x[kk + 1].reshape([r[kk+1], n[kk+1]*r[kk+2]], order='f'))
                        ).reshape([tr, n[kk+1], r[kk+2]], order='f')
            r[kk + 1] = copy.copy(tr)
        k = copy.copy(nk)

        # Swap dimensions
        c = (x[k].reshape([r[k]*n[k], r[k+1]], order='f').dot(
            x[k+1].reshape([r[k+1], n[k+1]*r[k+2]], order='f'))).reshape([r[k], n[k], n[k+1], r[k+2]], order='f')
        c = c.transpose([0, 2, 1, 3])
        U, s, V = np.linalg.svd(c.reshape([r[k]*n[k+1], n[k]*r[k+2]], order='f'), full_matrices=False)
        V = V.conj().T
        S = np.diag(s)
        r[k+1] = max(tt.utils.my_chop2(s, np.linalg.norm(s)*eps), 1)
        tmp = U.dot(S)
        x[k] = tmp[:, :r[k+1]].reshape([r[k], n[k+1], r[k+1]], order='f')
        x[k+1] = (V[:, :r[k+1]].conj().T).reshape([r[k+1], n[k], r[k+2]], order='f')
        idx[[k, k+1]] = idx[[k+1, k]]
        n[[k, k+1]] = n[[k+1, k]]
        k = max(k-1, 0) # check

    res = tt.vector.from_list(x)
    return res
