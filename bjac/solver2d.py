
import tt
import numpy as np
import copy
from tt.amen import amen_solve, amen_mv
import tt_tools as ttt

class Proto(object):
    pass

class Solver(object):

    def __init__(self, d, l, coef_funs, rhs, eps_op=1e-12, verb_multifun=1,
                 perm_x='transposed', perm_y='normal', prec_type='no A',
                 sol_exact_fun=None):

        self.d = d
        self.l = l
        self.n = 2 ** d
        self.h = 1. / self.n
        self.eps_op = eps_op
        self.grid = Proto()
        self.verb_multifun = verb_multifun
        self.perm_x = perm_x
        self.perm_y = perm_y

        self.get_grid()

        self.coef_funs = coef_funs
        self.get_coef()

        self.dV = self.h ** (2*(l+1)) #check
        self.A1 = -self.dV * self.coef1
        self.A2 = -self.dV * self.coef1

        if sol_exact_fun:
            self.sol_exact_fun = sol_exact_fun
            self.get_sol_exact()
        else:
            self.sol_exact_fun = None

        if callable(rhs):
            self.get_rhs(rhs)
        else:
            self.rhs = rhs

        self.get_B()
        self.get_N()

        self.prec_type = prec_type
        self.get_prec()

        self.get_Nrhs()

    def get_grid(self):
        d = self.d
        h = self.h
        l = self.l

        e = tt.ones(2, d=d)
        e2d = tt.ones(2, d=2*d)

        gr1d_shifted = h * (tt.xfun(2, d=d) + 0.5*e)
        gr1d = h * (tt.xfun(2, d=d) + e)

        if self.perm_x == 'normal':
            gr2d_x1 = tt.kron(e, gr1d)
            gr2d_x2 = tt.kron(gr1d, e)
            gr2d_x1_shifted = tt.kron(e, gr1d_shifted)
            gr2d_x2_shifted = tt.kron(gr1d_shifted, e)
        elif self.perm_x == 'transposed':
            gr2d_x1 = ttt.zkronv(e, gr1d)
            gr2d_x2 = ttt.zkronv(gr1d, e)
            gr2d_x1_shifted = ttt.zkronv(e, gr1d_shifted)
            gr2d_x2_shifted = ttt.zkronv(gr1d_shifted, e)
        else:
            raise Exception('Wrong permutation name. Must be either normal or transposed')
        if self.perm_y == 'normal':
            gr2d_y1 = tt.kron(e, gr1d)
            gr2d_y2 = tt.kron(gr1d, e)
            gr2d_y1_shifted = tt.kron(e, gr1d_shifted)
            gr2d_y2_shifted = tt.kron(gr1d_shifted, e)
        elif self.perm_y == 'transposed':
            gr2d_y1 = ttt.zkronv(e, gr1d)
            gr2d_y2 = ttt.zkronv(gr1d, e)
            gr2d_y1_shifted = ttt.zkronv(e, gr1d_shifted)
            gr2d_y2_shifted = ttt.zkronv(gr1d_shifted, e)
        else:
            raise Exception('Wrong permutation name. Must be either normal or transposed')

        grid_sol_1 = [[]] * (l+1)
        grid_sol_2 = [[]] * (l+1)
        for k in range(1, l+1):
            grid_sol_k1 = []
            grid_sol_k2 = []
            for i in range(k):
                if i == 0:
                    tmp1 = copy.deepcopy(gr2d_x1_shifted)
                    tmp2 = copy.deepcopy(gr2d_x2_shifted)
                else:
                    tmp1 = copy.deepcopy(gr2d_y1_shifted)
                    tmp2 = copy.deepcopy(gr2d_y2_shifted)
                for j in range(i):
                    tmp1 = tt.kron(e2d, tmp1)
                    tmp2 = tt.kron(e2d, tmp2)
                for j in range(i+1, k+1):
                    tmp1 = tt.kron(tmp1, e2d)
                    tmp2 = tt.kron(tmp2, e2d)
                grid_sol_k1.append(tmp1)
                grid_sol_k2.append(tmp2)
            grid_sol_1[k] = grid_sol_k1
            grid_sol_2[k] = grid_sol_k2

        for i in range(l+1):
            if i == 0:
                tmp1 = copy.deepcopy(gr2d_x1)
                tmp2 = copy.deepcopy(gr2d_x2)
            else:
                tmp1 = copy.deepcopy(gr2d_y1)
                tmp2 = copy.deepcopy(gr2d_y2)
            for j in range(i):
                tmp1 = tt.kron(e2d, tmp1)
                tmp2 = tt.kron(e2d, tmp2)
            grid_sol_1[i].append(tmp1)
            grid_sol_2[i].append(tmp2)

        self.grid.grid_sol_1 = grid_sol_1
        self.grid.grid_sol_2 = grid_sol_2

        grid_coef_1 = [tt.kron(gr2d_x1_shifted, tt.ones(2, d=2*l*d))]
        grid_coef_2 = [tt.kron(gr2d_x2_shifted, tt.ones(2, d=2*l*d))]
        for i in range(1, l+1):
            tmp1 = gr2d_y1_shifted
            tmp2 = gr2d_y2_shifted
            for j in range(i):
                tmp1 = tt.kron(e2d, tmp1)
                tmp2 = tt.kron(e2d, tmp2)
            for j in range(i+1, l+1):
                tmp1 = tt.kron(tmp1, e2d)
                tmp2 = tt.kron(tmp2, e2d)
            grid_coef_1.append(tmp1)
            grid_coef_2.append(tmp2)

        self.grid.grid_coef_1 = grid_coef_1
        self.grid.grid_coef_2 = grid_coef_2
        self.grid.x_shifted = [gr2d_x1_shifted, gr2d_x2_shifted]
        self.grid.x = [gr2d_x1, gr2d_x2]


    def get_sol_exact(self):
        self.sol_exact = []
        for i in range(self.l + 1):
            grid1 = self.grid.grid_sol_1[i]
            grid2 = self.grid.grid_sol_2[i]
            fun = lambda x: self.sol_exact_fun(x, i)
            sol_exact_i = tt.multifuncrs(grid1 + grid2, fun, self.eps_op,
                                         verb=self.verb_multifun
                                         ).round(self.eps_op)
            self.sol_exact.append(sol_exact_i)


    def get_coef(self):
        l = self.l

        self.coef1 = tt.multifuncrs(self.grid.grid_coef_1 + self.grid.grid_coef_2,
                                    lambda x: self.coef_funs[0](x[:, :l+1], x[:, l+1:]),
                                    self.eps_op, verb=self.verb_multifun).round(self.eps_op)

        self.coef2 = tt.multifuncrs(self.grid.grid_coef_1 + self.grid.grid_coef_2,
                                    lambda x: self.coef_funs[1](x[:, :l+1], x[:, l+1:]),
                                    self.eps_op, verb=self.verb_multifun).round(self.eps_op)


    def get_rhs(self, rhs):
        self.rhs = tt.multifuncrs(self.grid.x, lambda x: rhs(x), self.eps_op,
                                  verb=self.verb_multifun).round(self.eps_op)
        self.rhs = (self.h ** 2) * self.rhs


    def get_B(self):
        d = self.d
        l = self.l
        h = self.h
        eps_op = self.eps_op

        b = (tt.eye(2, d=d) - tt.qshift(d).T).round(eps_op)
        m = (tt.eye(2, d=d) + tt.qshift(d).T).round(eps_op)
        #m = tt.eye(2, d=d)

        delta_1n = (tt.matrix(tt.delta(2, d=d, center=0), n=[2]*d, m=[1]*d) *
                    tt.matrix(tt.delta(2, d=d, center=2**d-1), n=[1]*d, m=[2]*d)
                   ).round(eps_op)
        b_hash = b - delta_1n
        m_hash = m + delta_1n
        #m_hash = 1*m

        coef_corr = 1./np.sqrt(12)
        coef = .5
        #coef_corr = 0.
        #coef = 1.
        if self.perm_x == 'normal':
            C1 = coef/h * tt.kron(b, m)
            C2 = coef/h * tt.kron(m, b)
            C_corr = coef_corr/h * tt.kron(b, b)
        if self.perm_x == 'transposed':
            C1 = coef/h * ttt.zkronm(b, m)
            C2 = coef/h * ttt.zkronm(m, b)
            C_corr = coef_corr/h * ttt.zkronm(b, b)
        Cx = [C1, C2, C_corr]

        if self.perm_y == 'normal':
            C1 = coef/h * tt.kron(b_hash, m_hash)
            C2 = coef/h * tt.kron(m_hash, b_hash)
            C_corr = coef_corr/h * tt.kron(b_hash, b_hash)
        if self.perm_y == 'transposed':
            C1 = coef/h * ttt.zkronm(b_hash, m_hash)
            C2 = coef/h * ttt.zkronm(m_hash, b_hash)
            C_corr = coef_corr/h * ttt.zkronm(b_hash, b_hash)
        Cy = [C1, C2, C_corr]

        B = []
        for k in range(3):
            Bk = [tt.kron(Cx[k], tt.matrix(tt.ones(2, d=2*(l*d)), n=[2]*2*(l*d), m=[1]*2*(l*d)))]
            for i in range(1, l):
                _d = 2*((l-i)*d) #d[i+1:].sum()
                d_ = 2*(i*d) #d[:i].sum()
                Bi = tt.kron(Cy[k], tt.matrix(tt.ones(2, d=_d), n=[2]*_d, m=[1]*_d))
                Bi = tt.kron(tt.eye(2, d=d_), Bi).round(eps_op)
                Bk.append(Bi)
            Bk.append(tt.kron(tt.eye(2, d=2*(l*d)), Cy[k]).round(eps_op)) #here
            B.append(Bk)

        self.B = B

    def get_N(self):
        d = self.d
        l = self.l
        n = self.n
        eps_op = self.eps_op
        N0 = (tt.eye(2, d=d) - tt.diag(tt.delta(2, d=d, center=2**d-1))).round(eps_op)
        if self.perm_x == 'transposed':
            N = [ttt.zkronm(N0, N0)]
        elif self.perm_x == 'normal':
            N = [tt.kron(N0, N0)]
        for i in range(1, l+1):
            Ni = (tt.eye(2, d=2*d) - 1./n**2 * ttt.ones_matrix(2*d)).round(eps_op)
            # if self.perm_x == 'transposed':
            #     Ni = ttt.zkronm(Ni, Ni)
            # elif self.perm_x == 'normal':
            #     Ni = tt.kron(Ni, Ni)
            Ni = tt.kron(tt.eye(2, d=2*i*d), Ni).round(eps_op)
            N.append(Ni)

        self.N = N


    def get_prec(self):
        if self.prec_type == 'no A':
            A1 = - self.dV * tt.ones(self.A1.n)
            A2 = - self.dV * tt.ones(self.A2.n)
        elif self.prec_type == 'with A':
            A1 = self.A1
            A2 = self.A2
        B = self.B
        N = self.N
        d = self.d
        l = self.l
        eps_op = self.eps_op
        prec = []
        for i in range(l+1):
            prec_i_k = (B[0][i].T * tt.diag(A1)).round(eps_op)
            prec_i = (prec_i_k * B[0][i]).round(eps_op)
            prec_i_k = (B[2][i].T * tt.diag(A1)).round(eps_op)
            prec_i += (prec_i_k * B[2][i]).round(eps_op)
            prec_i = prec_i.round(eps_op)
            prec_i_k = (B[1][i].T * tt.diag(A2)).round(eps_op)
            prec_i += (prec_i_k * B[1][i]).round(eps_op)
            prec_i = prec_i.round(eps_op)
            prec_i_k = (B[2][i].T * tt.diag(A2)).round(eps_op)
            prec_i += (prec_i_k * B[2][i]).round(eps_op)
            prec_i = prec_i.round(eps_op)
            if i < l:
                prec_i_to_list = tt.matrix.to_list(prec_i)
                cores_left = prec_i_to_list[:2*d*(i+1)]
                cores_right_prod = reduce(lambda x, y: x*y, prec_i_to_list[2*d*(i+1):])
                cores_left[-1] *= cores_right_prod
                prec_i = tt.matrix.from_list(cores_left)
            prec_i = (prec_i * N[i]).round(eps_op)
            prec_i = (N[i].T * prec_i).round(eps_op)
            prec.append(prec_i)
        self.prec = prec


    def get_Nrhs(self):
        Nrhs = [self.rhs]
        N = self.N
        l = self.l
        d = self.d
        for i in range(1, l+1):
            Nrhs.append(0*tt.ones(2, d=2*d*(i+1)))
        Nrhs[0] = tt.matvec(N[0].T, Nrhs[0]).round(self.eps_op)
        self.Nrhs = Nrhs


    def get_sol2d(self, d_eps, tol):
        d = self.d
        if self.l == 1:
            u1 = ttt.prolongate_x(self.sol[1], d, d_eps, order=self.perm_x)
            if self.perm_x == 'transposed':
                perm1 = np.array(range(2*d_eps, 2*d_eps+d) + range(2*d_eps)[::2]).reshape(1, -1, order='f')
                perm2 = np.array(range(2*d_eps+d, 2*d_eps+2*d) + range(2*d_eps)[1::2]).reshape(1, -1, order='f')
                perm = np.concatenate((perm1, perm2), axis=0).flatten(order='f')
                u1 = ttt.permute(u1.round(tol), perm, tol).round(tol)

            elif self.perm_x == 'normal':
                perm = (range(2*d_eps, 2*d_eps+d) + range(d_eps) +
                        range(2*d_eps+d, 2*d_eps+2*d) + range(d_eps, 2*d_eps))
                u1 = ttt.permute(u1.round(tol), perm, tol).round(tol)
            else:
                raise Exception('Wrong permutation name. Must be either normal or transposed')
            u0 = ttt.prolongate_linear(self.sol[0], d + d_eps, order=self.perm_x, eps=1e-12)
            self.sol2d = (u0 + 1./2**d_eps * u1).round(tol)
            self.sol2d_u = [u0, u1]

            if self.sol_exact_fun:
                e = tt.ones(2, d=d+d_eps)
                y_eps = 2.**(-d) * (tt.xfun(2, d=d_eps+d) + e)
                def intfun(x): return x[:, 0] - np.array(x[:, 0], dtype=np.int32)
                y_eps = tt.multifuncrs2([y_eps], intfun, 1e-12, verb=self.verb_multifun).round(1e-12)

                x = 2.**(-d-d_eps) * (tt.xfun(2, d=d+d_eps) + e)

                if self.perm_x == 'normal':
                    kron = tt.kron
                elif self.perm_x == 'transposed':
                    kron = ttt.zkronv
                x1 = kron(e, x)
                x2 = kron(x, e)
                y1 = kron(e, y_eps)
                y2 = kron(y_eps, e)

                self.grid.x1_2d = x1
                self.grid.x2_2d = x2

                u1_exact = tt.multifuncrs2([x1, y1, x2, y2], lambda x: self.sol_exact_fun(x, 1), tol, verb=self.verb_multifun).round(tol)
                u0_exact = tt.multifuncrs2([x1, x2], lambda x: self.sol_exact_fun(x, 0), tol, verb=self.verb_multifun).round(tol)

                self.sol2d_exact = (u0_exact + 1./2**d_eps * u1_exact).round(tol)
                self.sol2d_u_exact = [u0_exact, u1_exact]

                print '||u0 - u0_exact||/||u0_exact|| = ', (self.sol2d_u_exact[0] - self.sol2d_u[0]).norm() / (self.sol2d_u_exact[0]).norm()
                print '||u1 - u1_exact||/||u1_exact|| = ', (self.sol2d_u_exact[1] - self.sol2d_u[1]).norm() / (self.sol2d_u_exact[1]).norm()
                print '||u - u_exact||/||u_exact|| = ', (self.sol2d_exact - self.sol2d).norm() / (self.sol2d_exact).norm()
        else:
            raise NotImplementedError

#def save(s, filename):


def matvec_NBABN(s, x, eps):
    A1 = s.A1.round(eps)
    A2 = s.A2.round(eps)
    B = s.B
    N = s.N
    d = s.d
    l = s.l

    # y = Nx
    y = []
    for i in range(l+1):
        y.append(tt.matvec(N[i], x[i]).round(eps))
        y[i] = tt.reshape(y[i], [2]*2*(i+1)*d + [1]*2*(l-i)*d)
        y[i].ps = np.array(y[i].ps, dtype=np.int32)

    # z = By
    z = []
    for k in range(2):
        zt = None
        for i in range(l+1):
            zt += tt.matvec(B[k][i], y[i])
            zt = zt.round(eps/l)
        z.append(zt.round(eps))

    # z = Az
    z[0] = (A1 * z[0]).round(eps)
    z[1] = (A2 * z[1]).round(eps)

    # y = B.T z + B.T A B y
    y_corr = []
    for i in range(l+1):
        y_corr_i = tt.matvec(B[2][i], y[i]).round(eps)
        y_corr_i1 = (A1 * y_corr_i).round(eps)
        y_corr_i2 = (A2 * y_corr_i).round(eps)
        y_corr_i = tt.matvec(B[2][i].T, (y_corr_i1 + y_corr_i2).round(eps)).round(eps)
        y_corr.append(y_corr_i)
    for i in range(l+1):
        yi = tt.matvec(B[0][i].T, z[0]).round(eps)
        yi += tt.matvec(B[1][i].T, z[1]).round(eps)
        y[i] = (yi + y_corr[i]).round(eps)

    # y = N y
    for i in range(l+1):
        inds = [range(2)]*2*(i+1)*d + [0]*2*(l-i)*d
        y[i] = tt.matvec(N[i].T, y[i][inds]).round(eps)
    return y


def prec_fun(s, x, eps, **kwargs):
    y = []
    for i in range(s.l+1):
        y.append(amen_solve(s.prec[i], x[i], x[i], eps, **kwargs).round(eps))
    for i in range(s.l+1):
        y[i] = tt.matvec(s.N[i], y[i]).round(eps)
    return y


def steepest_descent(s, eps, x0=None, maxiter=10, verb=1, rmax=40,
                     kickrank_prec=0, nswp_prec=2):
    l = s.l
    s.log = {'res': [], 'dx': []}
    if x0:
        x = copy.deepcopy(x0)
    else:
        x = copy.copy(s.Nrhs)
        x[0] = 0*x[0].round(eps)
    x_new = copy.copy(x)
    norm_Nrhs = s.Nrhs[0].norm()
    for k in range(maxiter):
        mvx = matvec_NBABN(s, x, eps) #eps/5
        resid = []
        err = []
        norm_resid = 0.
        for j in range(l+1):
            resid.append((mvx[j] - s.Nrhs[j]).round(eps, rmax=rmax))
            norm_resid += resid[j].norm()
        p = prec_fun(s, resid, eps, kickrank=kickrank_prec, nswp=nswp_prec)
        Ap = matvec_NBABN(s, p, eps) #eps/100
        tau_en = 0; tau_den = 0
        for i in range(l+1):
            tau_en += tt.dot(resid[i], p[i])
            tau_den += tt.dot(p[i], Ap[i])
        tau = tau_en / tau_den
        for i in range(l+1):
            x_new[i] = (x[i] - tau * p[i]).round(eps, rmax=rmax) #eps/5
        norm_dx = 0.
        for j in range(l+1):
            norm_dx += (x[j] - x_new[j]).norm() / (x_new[j].norm()+1e-16)
        if verb > 0:
            print('iter {0:2}, ||Ax-f||/||f|| = {1:.2e}, ||x_new-x||/||x||, {2:.2e}'.format(
                  k+1, norm_resid/norm_Nrhs, norm_dx))
        s.log['res'].append(norm_resid/norm_Nrhs)
        s.log['dx'].append(norm_dx)
        x = copy.copy(x_new)
        s.sol = copy.deepcopy(x)


def collection(key):
    if key == 'dim=2, l=1':
        def u0_fun(x): return 3./(2*np.sqrt(2)) * (x - np.log(1 + x)/np.log(2))
        def sol_exact_fun(x, i):
            if i == 0:
                return u0_fun(x[:, 0]) * u0_fun(x[:, 1])
            if i == 1:
                def du0_fun(x): return 3./(2*np.sqrt(2)) * (1 - 1./((1+x)*np.log(2)))
                def w_fun(x, C): return (1./(2*np.pi)*np.arctan(np.tan(2*np.pi*x)/np.sqrt(2)) - x + C)
                C1 = np.zeros(x[:, 1].shape)
                C1[(x[:, 1]>0.25) * (x[:, 1]<=0.75)] = .5
                C1[(x[:, 1]>0.75)] = 1.
                C2 = np.zeros(x[:, 3].shape)
                C2[(x[:, 3]>0.25) * (x[:, 3]<=0.75)] = .5
                C2[(x[:, 3]>0.75)] = 1.
                return 1.* (
                        u0_fun(x[:, 2]) * du0_fun(x[:, 0]) * w_fun(x[:, 1], C1) +
                        u0_fun(x[:, 0]) * du0_fun(x[:, 2]) * w_fun(x[:, 3], C2))
        # WARNING: RETURN BACK
        def coef_fun1d(x, y): return 2./3 * (1 + x) * (1 + np.cos(2*np.pi*y)**2)
        #def coef_fun1d(x, y): return (1 + 0.25*np.sin(2*np.pi*x)) * (1 + 0.25*np.sin(2*np.pi*y))
        def coef_fun(x1, x2): return (coef_fun1d(x1[:, 0], x1[:, 1]) *
                                     coef_fun1d(x2[:, 0], x2[:, 1]))
        # def coef_fun(x1, x2): return ((1 + 0.25*np.sin(2*np.pi*x1[:, 0])*np.sin(2*np.pi*x2[:, 0]))*
        #                               (1 + 0.25*np.sin(2*np.pi*x1[:, 1])*np.sin(2*np.pi*x2[:, 1])))
        def rhs_fun(x): return 1. * ((1 + x[:, 0]) * u0_fun(x[:, 0]) +
                                (1 + x[:, 1]) * u0_fun(x[:, 1]))
    else:
        raise NotImplementedError

    return rhs_fun, coef_fun, sol_exact_fun
