import tt
import numpy as np
import copy

def ones_matrix(d, n=2, m=2):
    cores = []
    for i in range(d):
        cores.append(np.ones((1, n, m, 1)))
    return tt.matrix.from_list(cores)


def permute(x_, order, eps):
    x = tt.vector.to_list(x_)
    d = copy.copy(x_.d)
    r = copy.copy(x_.r)
    n = copy.copy(x_.n)
    idx = np.ones(d, dtype=np.int)
    idx[order] = np.arange(d)
    eps = eps / d ** (1.5)
    if (len(order) < d):
        raise Exception('ORDER must have at least D elements for a D-dimensional tensor')

    for kk in range(d-1, 1, -1):
        Q, R = np.linalg.qr(x[kk].reshape([r[kk], n[kk]*r[kk+1]], order='f').T)
        tr = min([r[kk], n[kk]*r[kk+1]]);
        x[kk] = (Q.T).reshape([tr, n[kk], r[kk+1]], order='f')
        x[kk-1] = (x[kk-1].reshape([r[kk-1]*n[kk-1], r[kk]], order='f').dot(R.T)
                         ).reshape([r[kk-1], n[kk-1], tr], order='f')
        r[kk] = copy.copy(tr)

    k = 0
    while True:
        # Find next inversion
        nk = copy.copy(k)
        while (nk < d - 1) and (idx[nk] < idx[nk+1]):
            nk += 1
        if nk == d - 1:
            break

        # Move orthogonal centre there
        for kk in range(k, nk): # maybe to nk - 1
            Q, R = np.linalg.qr(x[kk].reshape([r[kk]*n[kk], r[kk+1]], order='f'))
            tr = min([r[kk]*n[kk], r[kk+1]])
            x[kk] = Q.reshape([r[kk], n[kk], tr], order='f')
            x[kk + 1] = (R.dot(x[kk + 1].reshape([r[kk+1], n[kk+1]*r[kk+2]], order='f'))
                        ).reshape([tr, n[kk+1], r[kk+2]], order='f')
            r[kk + 1] = copy.copy(tr)
        k = copy.copy(nk)

        # Swap dimensions
        c = (x[k].reshape([r[k]*n[k], r[k+1]], order='f').dot(
            x[k+1].reshape([r[k+1], n[k+1]*r[k+2]], order='f'))).reshape([r[k], n[k], n[k+1], r[k+2]], order='f')
        c = c.transpose([0, 2, 1, 3])
        U, s, V = np.linalg.svd(c.reshape([r[k]*n[k+1], n[k]*r[k+2]], order='f'), full_matrices=False)
        V = V.conj().T
        S = np.diag(s)
        r[k+1] = max(tt.utils.my_chop2(s, np.linalg.norm(s)*eps), 1)
        tmp = U.dot(S)
        x[k] = tmp[:, :r[k+1]].reshape([r[k], n[k+1], r[k+1]], order='f')
        x[k+1] = (V[:, :r[k+1]].conj().T).reshape([r[k+1], n[k], r[k+2]], order='f')
        idx[[k, k+1]] = idx[[k+1, k]]
        n[[k, k+1]] = n[[k+1, k]]
        k = max(k-1, 0) # check

    res = tt.vector.from_list(x)
    return res


def prolongate_x(t, d, d_new, order='normal'):
    if d == d_new:
        return t
    else:
        if d > d_new:
            raise Exception('d must be =< d_new')
        e = ones_matrix(d_new - d, n=2, m=1)
        I = tt.eye(2, d=d)
        P = tt.kron(e, I)
        if order == 'normal':
            P = tt.kron(P, P)
            t_list = tt.vector.to_list(t)
            t_new = tt.vector.from_list([np.ones((1, 1, 1))] * (d_new - d) + t_list[:d] +
                                        [np.ones((1, 1, 1))] * (d_new - d) + t_list[d:])
        elif order == 'transposed':
            P = zkronm(P, P)
            t_new = tt.vector.from_list([np.ones((1, 1, 1))] * 2*(d_new - d) + tt.vector.to_list(t))
        P = tt.kron(P, tt.eye(2, d=t.d - 2*d))
        res = tt.matvec(P, t_new).round(1e-14)
        return res


def prolongate_linear(t, d_new, order='normal', eps=1e-12):
    if t.d/2 > d_new:
        raise Exception('d must be =< d_new')
    elif t.d/2 == d_new:
        return t
    else:
        t_new = copy.deepcopy(t)
        dt = t_new.d / 2
        for i in range(d_new - dt):
            dt = t_new.d / 2
            cores = tt.vector.to_list(t_new)
            if order == 'normal':
                r1 = cores[1].shape[0]
                r2 = cores[1 + dt].shape[0]
                cores = (cores[:1] +
                         [np.eye(r1).reshape(r1, 1, r1, order='f')] + cores[1:dt+1] +
                         [np.eye(r2).reshape(r2, 1, r2, order='f')] + cores[dt+1:]
                        )
            elif order == 'transposed':
                r1 = cores[2].shape[0]
                r2 = cores[2].shape[0]
                cores = (cores[:2] +
                         [np.eye(r1).reshape(r1, 1, r1, order='f')] +
                         [np.eye(r2).reshape(r2, 1, r2, order='f')] + cores[2:]
                        )
            t_new = tt.vector.from_list(cores)
            t_new = tt.matvec(qtt_prolongation([dt]*2, order=order), t_new).round(eps)
        return t_new


def qtt_prolongation(d, corr=0, order='normal'):
    '''
    Builds D-dimensional prolongation operator from
    2^d[0] o ... o 2^d[D] to 2^(d[0]+1) o ... o 2^(d[D]+1)
    grid in the QTT format
    '''
    a = np.array([
                [0.5, 0],
                [1., 0],
                [0.5, 0.5],
                [0, 1.]
        ])

    b = np.array([
                [0, 0.5],
                [0, 0],
                [0, 0],
                [0, 0]
        ])

    c = np.array([
                [corr, 0],
                [0, 0],
                [0, 0],
                [0, 0]
        ])

    a = a.reshape([2, 2, 2, 1], order='f')
    b = b.reshape([2, 2, 2, 1], order='f')
    c = c.reshape([2, 2, 2, 1], order='f')

    a = tt.matrix(a)
    b = tt.matrix(b)
    c = tt.matrix(c)

    if order == 'normal':
        kron = tt.kron
    elif order == 'transposed':
        kron = zkronm

    D = len(d)
    P = tt.eye([1])
    for i in range(D):
        I = tt.eye([2] * (d[i] - 1))
        if d[i] != 2: #qshift is bugged when d = 2 !!!
            J = tt.qshift(d[i] - 1).T
        else:
            J = np.array([[0., 0.], [1., 0.]])
            J = tt.matrix(J)
        if corr == 0:
            p = (tt.kron(a, I) + tt.kron(b, J)).round(1e-14)
        else:
            p = (tt.kron(a, I) + tt.kron(b, J) + tt.kron(c, tt.diag(tt.delta(2, d=d[i]-1, center=0)))).round(1e-14)
        if i == 0:
            P = copy.deepcopy(p)
        else:
            P = kron(P, p)
    return P


def zkronv(ttA, ttB, eps=1e-12):
    Al = tt.vector.to_list(ttA)
    Bl = tt.vector.to_list(ttB)
    Hl = []
    for (A, B) in zip(Al, Bl):
        H = np.kron(B, A)
        Hl.append(H)
    ttH = tt.vector.from_list(Hl)
    sz = np.concatenate((ttA.n, ttB.n))
    res = tt.reshape(ttH, sz, eps)
    res.ps = np.array(res.ps, dtype=np.int32)
    res = res.round(eps)
    return res


def zkronm(ttA, ttB, eps=1e-12):
    Al = tt.matrix.to_list(ttA)
    Bl = tt.matrix.to_list(ttB)
    Hl = []
    for (A, B) in zip(Al, Bl):
        H = np.kron(B, A)
        Hl.append(H)
    res = tt.matrix.from_list(Hl)
    szn = np.concatenate((ttA.n.reshape(1, -1), ttB.n.reshape(1, -1))).flatten(order='f')
    szm = np.concatenate((ttA.m.reshape(1, -1), ttB.m.reshape(1, -1))).flatten(order='f')
    res = tt.reshape(res, np.array([szn, szm]).T, eps)
    res.tt.ps = np.array(res.tt.ps, dtype=np.int32)
    res = res.round(eps)
    return res
