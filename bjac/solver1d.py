
import tt
import numpy as np
import copy
from tt.amen import amen_solve, amen_mv

class Proto(object):
    pass

class Solver(object):

    def __init__(self, d, coef_fun, rhs, sol_exact_fun=False, dsol_exact_fun=False,
                 eps_op=1e-12, verb_multifun=1, order='xy1...yn'):

        self.d = d
        self.l = len(d) - 1
        self.n = 2 ** d
        self.h = 1. / self.n
        self.eps_op = eps_op
        self.dV = reduce(lambda x, y: x*y, self.h)
        self.grid = Proto()
        self.get_grid()
        self.verb_multifun = verb_multifun

        self.coef_fun = coef_fun
        self.coef = tt.multifuncrs(self.grid.grid_coef, lambda x: self.coef_fun(x),
                                   self.eps_op, verb=verb_multifun).round(self.eps_op)
        self.dV = reduce(lambda x, y: x*y, self.h)
        self.A = -self.dV * self.coef
        self.order = order

        if sol_exact_fun:
            self.sol_exact_fun = sol_exact_fun
            self.dsol_exact_fun = dsol_exact_fun
            self.get_exact_sol()


        self.rhs = self.h[0] * rhs
        self.B = get_B(self.d, self.l, self.h, self.eps_op, order=self.order)
        self.N = get_N(self.d, self.l, self.n, self.eps_op, order=self.order)
        self.Nrhs = get_Nrhs(self.rhs, self.N, self.l, self.d)
        self.prec = get_prec(self.A, self.B, self.N,
                             self.d, self.l, self.eps_op, self.order)


    def iterate(self, eps, x0=None, maxiter=20, verb=1, rmax=40):
        self.eps = eps
        self.sol = steepest_descent(self.A, self.B, self.N, self.prec,
                                    self.Nrhs, self.l, self.d, self.eps, self.order,
                                    x0=x0, maxiter=maxiter, verb=verb, rmax=rmax)

    def get_grid(self):
        d = self.d
        h = self.h
        l = self.l

        e = []
        gr1d = []
        gr1d_shifted = []
        for i in range(l+1):
            e.append(tt.ones(2, d=d[i]))
            gr1d_shifted.append(h[i] * (tt.xfun(2, d=d[i]) + 0.5*tt.ones(2, d=d[i])))
            gr1d.append(h[i] * (tt.xfun(2, d=d[i]) + tt.ones(2, d=d[i])))

        grid_sol = [[]] * (l+1)
        for k in range(1, l+1):
            grid_sol_k = []
            for i in range(k):
                tmp = gr1d_shifted[i]
                for j in range(i):
                    tmp = tt.kron(e[j], tmp)
                for j in range(i+1, k+1):
                    tmp = tt.kron(tmp, e[j])
                grid_sol_k.append(tmp)
            grid_sol[k] = grid_sol_k

        for i in range(l+1):
            tmp = gr1d[i]
            for j in range(i):
                tmp = tt.kron(e[j], tmp)
            grid_sol[i].append(tmp)

        grid_coef = []
        for i in range(l+1):
            tmp = gr1d_shifted[i]
            for j in range(i):
                tmp = tt.kron(tt.ones(2, d=d[j]), tmp)
            for j in range(i+1, l+1):
                tmp = tt.kron(tmp, tt.ones(2, d=d[j]))
            grid_coef.append(tmp)

        self.grid.grid_coef = grid_coef[::-1]#grid_coef here
        self.grid.grid_sol = grid_sol

    def get_exact_sol(self):
        self.sol_exact = []
        self.dsol_exact = []
        for i in range(self.l + 1):
            grid = self.grid.grid_sol[i]
            fun = lambda x: self.sol_exact_fun(x, i)
            sol_exact_i = tt.multifuncrs(grid, fun, self.eps_op,
                                         verb=self.verb_multifun
                                         ).round(self.eps_op)
            self.sol_exact.append(sol_exact_i)

            dfun = lambda x: self.dsol_exact_fun(x, i)
            dsol_exact_i = tt.multifuncrs(grid, dfun, self.eps_op,
                                         verb=self.verb_multifun
                                         ).round(self.eps_op)
            self.dsol_exact.append(dsol_exact_i)


    def get_sol_x(self, sol, d_delta, eps, interp_ui_x='const', corr=0):
        # eps = 1. / (2 ** d_delta + 1) - multiscale parameter
        d = self.d
        u0 = sol[0]
        u1 = sol[1]
        sol_x_u0 = copy.copy(u0)
        for i in range(d_delta - d[0] + d[1]):
            sol_x_u0 = prolongate(sol_x_u0, corr=corr)

        r = u1.r[d[0]]
        cores = tt.vector.to_list(u1)
        cl = cores[:d[0]]
        cr = cores[d[0]:]
        sol_x_u1 = None

        for i in range(r):
            cl_i = cl[:-1] + [cl[-1][:, :, i].reshape(-1, 2, 1, order='f')]
            cr_i = [cr[0][i, :, :].reshape(1, 2, -1, order='f')] + cr[1:]
            u_i = tt.vector.from_list(cl_i)
            if interp_ui_x == 'linear':
                for i in range(d_delta - d[0]):
                    u_i = prolongate(u_i, corr=0.5)
            elif interp_ui_x == 'const':
                u_i = prolongate_constant(u_i, d_delta)
            cl_i = tt.vector.to_list(u_i)

            sol_x_u1 += tt.vector.from_list(cr_i + cl_i)
            sol_x_u1 = sol_x_u1.round(1e-14)

        self.d_delta = d_delta
        self.delta = 1. / (2 ** (d_delta))
        self.sol_x_ui = [sol_x_u0, sol_x_u1]
        sol_x = (sol_x_u0 + self.delta * sol_x_u1).round(eps)
        self.sol_x = sol_x
        return sol_x

def h1_error(self):
    h = self.h
    d = self.d
    l = self.l
    eps_op = self.eps_op
    eps = self.eps
    du_exact = self.dsol_exact[0]
    du1_exact = self.dsol_exact[1]
    sol = self.sol

    b = [1. / h[0] * (tt.eye(2, d=d[0]) - tt.qshift(d[0]).T).round(eps_op)]
    for i in range(1, l+1):
        bi = (tt.eye(2, d=d[i]) - tt.qshift(d[i]).T).round(eps_op)
        delta_1n = (tt.matrix(tt.delta(2, d=d[i], center=0), n=[2]*d[i], m=[1]*d[i]) *
                    tt.matrix(tt.delta(2, d=d[i], center=2**d[i]-1), n=[1]*d[i], m=[2]*d[i])
                   ).round(eps_op)
        bi = 1./ h[i] * (bi - delta_1n).round(eps_op)
        b.append(bi)
    diff_qtt = []
    for i in range(l+1):
        tmp = b[i]
        for j in range(i):
            tmp = tt.kron(tt.eye(2, d=d[j]), tmp)
        diff_qtt.append(tmp)

    dsol = []
    for i in range(l+1):
        sol_i = sol[i].round(eps)
        dsol_i = tt.matvec(diff_qtt[i], sol_i).round(eps)
        dsol.append(dsol_i)

    err_h1_d = np.sqrt(h[0])*(dsol[0] - du_exact).norm()+ np.sqrt(h[0]*h[1])*(dsol[1] - du1_exact).norm()
    return err_h1_d


def h1_error_full(self, int_fprime2_exact):
    h = self.h
    sol_full = np.concatenate(([0], self.sol[0].full().flatten(order='f')))
    sol_exact_full = np.concatenate(([0], self.sol_exact[0].full().flatten(order='f')))
    h1_1 = 1./h[0] * (sol_full[1:-1]*(2 * sol_exact_full[1:-1] - sol_exact_full[:-2] - sol_exact_full[2:])).sum()
    h1_2 = 2./h[0] * (sol_full[1:]**2 - sol_full[1:]*sol_full[:-1]).sum()
    return np.sqrt(int_fprime2_exact - 2*h1_1 + h1_2)

def collection(key):
    if key == 'dim=1, l=1':

        def sol_exact_fun(x, i):
            if i == 0:
                return 3./(2*np.sqrt(2)) * (x[:, 0] - np.log(1 + x[:, 0])/np.log(2))
            if i == 1:
                C = np.zeros(x[:, 1].shape)
                C[(x[:, 1]>0.25) * (x[:, 1]<=0.75)] = .5
                C[(x[:, 1]>0.75)] = 1.
                return (3./(2*np.sqrt(2)) *
                       (1 - 1./((1+x[:, 0])*np.log(2))) *
                       (1./(2*np.pi)*np.arctan(np.tan(2*np.pi*x[:, 1])/np.sqrt(2))
                        - x[:, 1] + C))

        def dsol_exact_fun(x, i):
            if i == 0:
                return 3./(2*np.sqrt(2)) * (1 - 1./(1 + x[:, 0])/np.log(2))
            if i == 1:
                return (3./(2*np.sqrt(2)) *
                       (1 - 1./((1+x[:, 0])*np.log(2))) *
                       ( np.sqrt(2) / (1 + np.cos(2*np.pi*x[:, 1])**2) - 1))

        # FIX TO THE CORRECT COEF FUN
        def coef_fun(x):
            return 2./3 * (1 + x[:, 0]) * (1 + np.cos(2*np.pi*x[:, 1])**2)

        # def coef_fun(x):
        #     return  (1 + 0.5*np.sin(2*np.pi*x[:, 0])) * (1 + 0.5*np.sin(2*np.pi*x[:, 1]))
    else:
        raise NotImplementedError

    return coef_fun, sol_exact_fun, dsol_exact_fun


def get_B(d, l, h, eps_op, order='xy1...yn'):

    b = [1. / h[0] * (tt.eye(2, d=d[0]) - tt.qshift(d[0]).T).round(eps_op)]
    for i in range(1, l+1):
        bi = (tt.eye(2, d=d[i]) - tt.qshift(d[i]).T).round(eps_op)
        delta_1n = (tt.matrix(tt.delta(2, d=d[i], center=0), n=[2]*d[i], m=[1]*d[i]) *
                    tt.matrix(tt.delta(2, d=d[i], center=2**d[i]-1), n=[1]*d[i], m=[2]*d[i])
                   ).round(eps_op)
        bi = 1./ h[i] * (bi - delta_1n).round(eps_op)
        b.append(bi)

    if order == 'xy1...yn':
        B = [tt.kron(b[0], tt.matrix(tt.ones(2, d=d[1:].sum()), n=[2]*d[1:].sum(), m=[1]*d[1:].sum()))]
        for i in range(1, l):
            _d = d[i+1:].sum()
            d_ = d[:i].sum()
            Bi = tt.kron(b[i], tt.matrix(tt.ones(2, d=_d), n=[2]*_d, m=[1]*_d))
            Bi = tt.kron(tt.eye(2, d=d_), Bi).round(eps_op)
            B.append(Bi)
        B.append(tt.kron(tt.eye(2, d=d[:-1].sum()), b[-1]).round(eps_op)) #here
    elif order == 'yn...y1x':
        B = [tt.kron(tt.matrix(tt.ones(2, d=d[1:].sum()), n=[2]*d[1:].sum(), m=[1]*d[1:].sum()), b[0])]
        for i in range(1, l):
            _d = d[i+1:].sum()
            d_ = d[:i].sum()
            Bi = tt.kron(tt.matrix(tt.ones(2, d=_d), n=[2]*_d, m=[1]*_d), b[i])
            Bi = tt.kron(Bi, tt.eye(2, d=d_)).round(eps_op)
            B.append(Bi)
        B.append(tt.kron(b[-1], tt.eye(2, d=d[:-1].sum())).round(eps_op)) #here
    else:
        raise NotImplementedError

    return B



def get_N(d, l, n, eps_op, order='xy1...yn'):
    N = [(tt.eye(2, d=d[0]) - tt.diag(tt.delta(2, d=d[0], center=2**d[0]-1))).round(eps_op)]
    if order == 'xy1...yn':
        for i in range(1, l+1):
            Ni = tt.eye(2, d=d[:i].sum())
            Ni = 1./n[i] * tt.kron(Ni, tt_ones_matrix(d[i])).round(eps_op)
            Ni = (tt.eye(Ni.n) - Ni).round(eps_op)
            N.append(Ni)
    elif order == 'yn...y1x':
        for i in range(1, l+1):
            Ni = tt.eye(2, d=d[:i].sum())
            Ni = 1./n[i] * tt.kron(tt_ones_matrix(d[i]), Ni).round(eps_op)
            Ni = (tt.eye(Ni.n) - Ni).round(eps_op)
            N.append(Ni)
    return N


def get_prec(A, B, N, d, l, eps_op, order):
    prec = []
    for i in range(l+1):
        prec_i = (B[i].T * tt.diag(A))
        prec_i = (prec_i * B[i]).round(eps_op)
        if order == 'xy1...yn':
            prec_i = (prec_i * N[i]).round(eps_op)
            prec_i = (N[i].T * prec_i).round(eps_op)
        elif order == 'yn...y1x': #check why order changes
            if i < l:
                prec_i_to_list = tt.matrix.to_list(prec_i)
                cores_right = prec_i_to_list[-d[:i+1].sum():]
                cores_left_prod = reduce(lambda x, y: x*y, prec_i_to_list[:-d[:i+1].sum()])
                cores_right[-1] *= cores_left_prod
                prec_i = tt.matrix.from_list(cores_right)
            prec_i = (prec_i * N[i]).round(eps_op)
            prec_i = (N[i].T * prec_i).round(eps_op)
        # when multiplying (2**d x 2**D) * (2**D x 2**d), D>d
        # tt returns D-dim matrix with (D-d) n=1, m=1
        # hence, need to reduce the dimenension:
        if order == 'xy1...yn':
            if i < l:
                prec_i_to_list = tt.matrix.to_list(prec_i)
                cores_left = prec_i_to_list[:d[:i+1].sum()]
                cores_right_prod = reduce(lambda x, y: x*y, prec_i_to_list[d[:i+1].sum():])
                cores_left[-1] *= cores_right_prod
                prec_i = tt.matrix.from_list(cores_left)
        prec.append(prec_i)
    return prec


def get_Nrhs(rhs, N, l, d):
    Nrhs = [rhs]
    for i in range(1, l+1):
        Nrhs.append(0*tt.ones(2, d=d[:i+1].sum()))
    for i in range(l+1):
        Nrhs[i] = tt.matvec(N[i].T, Nrhs[i])
    return Nrhs


def matvec_NBABN(A, B, N, x, l, d, eps, order):
    # y = Nx
    y = []
    for i in range(l+1):
        y.append(tt.matvec(N[i], x[i]).round(eps))
        if order == 'xy1...yn':
            y[i] = tt.reshape(y[i], [2]*d[:i+1].sum() + [1]*d[i+1:].sum())
        elif order == 'yn...y1x':
            y[i] = tt.reshape(y[i], [1]*d[i+1:].sum() + [2]*d[:i+1].sum())
        else:
            raise NotImplementedError
        y[i].ps = np.array(y[i].ps, dtype=np.int32)

    # z = By
    z = None
    for i in range(l+1):
        z += tt.matvec(B[i], y[i])
        z = z.round(eps/l)
    z = z.round(eps)

    # z = Az
    z = (A * z).round(eps)
    #z, _ = amen_mv(A, z, eps, y=z, verb=0, kickrank=0, nswp=4)

    # z = B.T z
    for i in range(l+1):
        y[i] = tt.matvec(B[i].T, z).round(eps)

    # y = N y
    for i in range(l+1):
        if order == 'xy1...yn':
            inds = [range(2)]*d[:i+1].sum() + [0]*d[i+1:].sum()
        elif order == 'yn...y1x':
            inds = [0]*d[i+1:].sum() + [range(2)]*d[:i+1].sum()
        else:
            raise NotImplementedError
        #y[i] = tt.reshape(y[i], [2]*d[:i+1].sum())
        #y[i].ps = np.array(y[i].ps, dtype=np.int32)
        y[i] = tt.matvec(N[i].T, y[i][inds]).round(eps)
    return y


def prec_fun(prec, N, x, l, eps, d, **kwargs):
    y = []
    for i in range(l+1):
        y.append(amen_solve(prec[i], x[i], x[i], eps, **kwargs).round(eps))
    for i in range(l+1):
        y[i] = tt.matvec(N[i], y[i]).round(eps)
#     y = []
#     for i in range(l+1):
#         prec_i_full = prec[i].full()
#         prec_i_full = np.linalg.pinv(prec_i_full).reshape([2]*2*d[:i+1].sum(), order='f')
#         prec_i = tt.matrix(prec_i_full, eps=eps, n=[2]*d[:i+1].sum(), m=[2]*d[:i+1].sum())
#         y.append(tt.matvec(prec_i, x[i]).round(eps))
#         y[i] = tt.matvec(N[i], y[i]).round(eps)
    return y


def tt_ones_matrix(d):
    cores = []
    for i in range(d):
        cores.append(np.ones((1, 2, 2, 1)))
    return tt.matrix.from_list(cores)


def steepest_descent(A, B, N, prec, Nrhs, l, d, eps, order,
                     x0=None, maxiter=10, verb=1, rmax=40):
    if x0:
        x = copy.deepcopy(x0)
    else:
        x = copy.copy(Nrhs)
        x[0] = 0*x[0].round(eps)
    x_new = copy.copy(x)
    norm_Nrhs = Nrhs[0].norm()
    for k in range(maxiter):
        mvx = matvec_NBABN(A, B, N, x, l, d, eps, order) #eps/5
        resid = []
        err = []
        norm_resid = 0.
        for j in range(l+1):
            resid.append((mvx[j] - Nrhs[j]).round(eps, rmax=rmax))
            norm_resid += resid[j].norm()
        p = prec_fun(prec, N, resid, l, eps, d, kickrank=0, nswp=2)
        Ap = matvec_NBABN(A, B, N, p, l, d, eps, order) #eps/100
        tau_en = 0; tau_den = 0
        for i in range(l+1):
            tau_en += tt.dot(resid[i], p[i])
            tau_den += tt.dot(p[i], Ap[i])
        tau = tau_en / tau_den
        for i in range(l+1):
            x_new[i] = (x[i] - tau * p[i]).round(eps, rmax=rmax) #eps/5
        norm_dx = 0.
        norm_x = 0.
        for j in range(l+1):
            norm_x += x_new[j].norm()
            norm_dx += (x[j] - x_new[j]).norm()
        if verb > 0:
            print('iter {0:2}, ||Ax-f||/||f|| = {1:.2e}, ||x_new-x||/||x||, {2:.2e}'.format(
                  k+1, norm_resid/norm_Nrhs, norm_dx/norm_x))
        x = copy.copy(x_new)
    return x


def qtt_prolongation(d, corr=0):
    '''
    Builds D-dimensional prolongation operator from
    2^d[0] o ... o 2^d[D] to 2^(d[0]+1) o ... o 2^(d[D]+1)
    grid in the QTT format
    '''
    a = np.array([
                [0.5, 0],
                [1., 0],
                [0.5, 0.5],
                [0, 1.]
        ])

    b = np.array([
                [0, 0.5],
                [0, 0],
                [0, 0],
                [0, 0]
        ])

    c = np.array([
                [corr, 0],
                [0, 0],
                [0, 0],
                [0, 0]
        ])

    a = a.reshape([2, 2, 2, 1], order='f')
    b = b.reshape([2, 2, 2, 1], order='f')
    c = c.reshape([2, 2, 2, 1], order='f')

    a = tt.matrix(a)
    b = tt.matrix(b)
    c = tt.matrix(c)

    D = len(d)
    P = tt.eye([1])
    for i in range(D):
        I = tt.eye([2] * (d[i] - 1))
        if d[i] != 2: #qshift is bugged when d = 2 !!!
            J = tt.qshift(d[i] - 1).T
        else:
            J = np.array([[0., 0.], [1., 0.]])
            J = tt.matrix(J)
        if corr == 0:
            p = (tt.kron(a, I) + tt.kron(b, J)).round(1e-14)
        else:
            p = (tt.kron(a, I) + tt.kron(b, J) + tt.kron(c, tt.diag(tt.delta(2, d=d[i]-1, center=0)))).round(1e-14)
        P = tt.kron(P, p)
    return P


def prolongate(t, corr=0, eps=1e-12):
    # first need to increase dimension of input as follows:
    # n(0) = 2, n(1) =2, n(2) = 2 ->
    # n(0) = 1, n(1) =2, n(2) = 1, n(3) = 2, n(4) = 2
    cores = tt.vector.to_list(t)
    r = cores[1].shape[0]
    cores = [np.ones(1).reshape(1, 1, 1)] + cores
    cores = cores[:2] + [np.eye(r).reshape(r, 1, r, order='f')] + cores[2:]
    t_new = tt.vector.from_list(cores)
    Pt = tt.matvec(qtt_prolongation([t.d], corr=corr), t_new).round(eps)
    return Pt[[0] + [range(2)]*(t.d+1)]


def prolongate_constant(x, d_new):
    d = x.d
    if d_new <= d:
        raise Exception('d_new must be > d')
    t = copy.deepcopy(x)
    for i in range(d_new - d):
        d_curr = d + i
        e = tt.matrix(np.ones((2, 1)))
        I = tt.eye(2, d=d_curr)
        P = tt.kron(e, I)
        t = tt.vector.from_list([np.ones((1, 1, 1))] + tt.vector.to_list(t))
        t = tt.matvec(P, t).round(1e-14)
    return t
